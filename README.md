# [Timelines](https://thesilkminer.gitlab.io/timelines)

The official repository of the open source website dedicated to timelines for
beloved franchises, with some additional content sprinkled on top.

This website is an ongoing effort that aims to provide release dates and
potentially in-universe ordering of video games, comics, novels, and other
media that populates a particular franchise's universe. The website is also
fully open source, allowing anyone to contribute with either error corrections
or additional information.

This website also takes inspiration from [All Timelines](https://alltimelines.com/)
(formerly [MythBank](https://mythbank.com/)), although the goals of the two platforms
are fundamentally different.

## The Content of this repository
This repository is split into three parts: the actual website content, the
custom Static Site Generator developed for this project, and the test server
launcher. The test server launcher exists merely to support local development
and testing, and spawns a Ktor web server configured to serve the correct
content.

The entire build process is managed by Gradle and a custom set of plugins
defined for it.

## A word on Licensing
Due to the nature of this project, licensing was not going to be easy. For this
reason, the project is subject to split licensing depending on which section
you are looking at. The main `COPYING` and `COPYING.LESSER` files cover the
whole repo, except for the `content` subdirectory, where the license is now
determined by both `content/LICENSE` and `content/3RD_PARTY_LICENSES.md`,
depending on the file being targeted.

I hope this does not make the repository structure and the licensing situation
too complex.
