import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.31"
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_16.majorVersion))
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(group = "io.ktor", name = "ktor-server-core", version = "1.6.5")
    implementation(group = "io.ktor", name = "ktor-server-netty", version = "1.6.5")
    implementation(group = "ch.qos.logback", name = "logback-classic", version = "1.2.5")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_16.toString()
        freeCompilerArgs = listOf("-Xjvm-default=all", "-Xlambdas=indy")
    }
}
