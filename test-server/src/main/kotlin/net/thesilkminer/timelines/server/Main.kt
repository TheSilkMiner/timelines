@file:JvmName("0_Main")

package net.thesilkminer.timelines.server

import io.ktor.application.install
import io.ktor.http.content.files
import io.ktor.http.content.static
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.engine.ShutDownUrl
import io.ktor.server.netty.Netty

fun main(args: Array<String>) {
    println("Booting test server with root '${args[1]}' reading static files from '${args[0]}' and shutdown url '/shutdown'")
    embeddedServer(Netty, port = 8080) {
        install(ShutDownUrl.ApplicationCallFeature) {
            shutDownUrl = "/shutdown"
            exitCodeSupplier = { 0 }
        }
        routing {
            static("/${args[1]}") {
                files(args[0])
            }
        }
    }.start(wait = true)
}
