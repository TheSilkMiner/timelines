plugins {
    groovy
    id("com.github.ben-manes.versions") version "0.42.0"
    id("com.github.hierynomus.license") version "0.16.1"
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_16.majorVersion))
}

license {
    header = file("NOTICE")
    strictCheck = true
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(group = "org.codehaus.groovy", name = "groovy", version = "3.0.9")
    implementation(group = "org.codehaus.groovy", name = "groovy-json", version = "3.0.9")
}

tasks {
    val compileGroovy by getting
    val licenseFormat by getting
    compileGroovy.dependsOn(licenseFormat)

    withType<GroovyCompile> {
        groovyOptions.optimizationOptions?.put("indy", true)
    }
}
