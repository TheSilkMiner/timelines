/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg

import groovy.transform.PackageScope

import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes

@PackageScope
class CopyingFileVisitor extends SimpleFileVisitor<Path> {
    private final Path input
    private final Path output

    @PackageScope
    CopyingFileVisitor(final Path input, final Path output) {
        this.input = input ?: { throw new IllegalStateException("Invalid input path") }()
        this.output = output ?: { throw new IllegalStateException("Invalid output path") }()
    }

    @Override
    FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) {
        Objects.requireNonNull dir
        Objects.requireNonNull attrs
        Files.createDirectories this.output.resolve(this.input.relativize(dir))
        super.preVisitDirectory dir, attrs
    }

    @Override
    FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
        Objects.requireNonNull file
        Objects.requireNonNull attrs
        Files.copy file, this.output.resolve(this.input.relativize(file))
        super.visitFile file, attrs
    }
}
