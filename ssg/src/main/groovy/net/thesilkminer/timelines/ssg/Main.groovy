/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg

import java.nio.file.Paths

class Main {
    static void main(final String... args) {
        println("Initializing SSG with static '${args[1]}', template '${args[2]}', data '${args[0]}', output '${args[3]}' for website root '${args[4]}'")

        final dataDirectory = Paths.get args[0]
        final staticDirectory = Paths.get args[1]
        final templateDirectory = Paths.get args[2]
        final outputDirectory = Paths.get args[3]
        final websiteRoot = args[4]

        final job = new StaticSiteGeneratorJob(dataDirectory, staticDirectory, templateDirectory, outputDirectory, websiteRoot)
        job.run()
    }
}
