/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating

import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderCommands
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderReplacer

@PackageScope
class CustomHtmlCreator extends SimpleFileCreator {
    private static final Map<String, Closure<?>> CUSTOM_HTML_COMMANDS = [:]

    @Override
    void makeContents(final StringBuilder builder, final PlaceholderReplacer replacer, final TemplateContext context, final String[] template, final String[] file, final PageData data) {
        final PlaceholderReplacer subReplacer = new PlaceholderReplacer(PlaceholderCommands.ALL + CUSTOM_HTML_COMMANDS)
        template.each {
            replacer.replacingPlaceholders(builder, it, context, data) { innerBuilder -> writeFile(subReplacer, context, data, innerBuilder, file) }
        }
    }

    private static void writeFile(final PlaceholderReplacer replacer, final TemplateContext context, final PageData data, final StringBuilder builder, final String[] file) {
        file.drop(4).each {
            replacer.replacingPlaceholders(builder, it, context, data) {
                throw new IllegalStateException('Invalid {{ page-content }} command found in file: they are leaf nodes')
            }
            if (!it.endsWith('>')) builder << ' '
        }
    }
}
