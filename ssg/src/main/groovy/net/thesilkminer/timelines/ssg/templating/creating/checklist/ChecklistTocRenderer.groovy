/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist

import groovy.transform.PackageScope

@PackageScope
class ChecklistTocRenderer {
    def renderContents(final StringBuilder builder, final contents) {
        this.renderContentList(builder, contents)
    }

    private renderContentList(final StringBuilder builder, final List contents) {
        if (contents.any { it instanceof Map }) {
            builder << '<ul>'
            contents.each { this.render(builder, it) }
            builder << '</ul>'
        }
    }

    private render(final StringBuilder builder, final Map section) {
        builder << '<li><a href="#tl-100-checklist--anchor--'
        builder << section.id.toLowerCase(Locale.ENGLISH).replace([ ' ' : '' ]) << '">' << section.id << '</a>'
        this.renderContents(builder, section.contents)
        builder << '</li>'
    }

    @SuppressWarnings('unused')
    private render(final StringBuilder builder, final def section) {}
}
