/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating

class TemplateContext {
    final String websiteRoot

    private final FileCache cache

    TemplateContext(final String websiteRoot, final FileCache cache) {
        this.websiteRoot = websiteRoot
        this.cache = cache
    }

    def url(final String... arguments) {
        final targetUrl = [this.websiteRoot, *arguments.collect { it.startsWith('/')? it.substring(1) : it }].join '/'
        targetUrl.replace '//', '/'
    }

    String[] lookupFile(final String file) {
        this.cache[file]
    }
}
