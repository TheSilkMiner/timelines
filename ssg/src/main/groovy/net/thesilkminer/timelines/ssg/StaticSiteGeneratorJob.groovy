/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg

import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.TemplateEngine

import java.nio.file.Files
import java.nio.file.Path

@PackageScope
class StaticSiteGeneratorJob implements Runnable {

    private final Path dataDirectory
    private final Path staticDirectory
    private final Path templateDirectory
    private final Path outputDirectory
    private final String websiteRoot

    @PackageScope
    StaticSiteGeneratorJob(
            final Path dataDirectory,
            final Path staticDirectory,
            final Path templateDirectory,
            final Path outputDirectory,
            final String websiteRoot
    ) {
        this.dataDirectory = dataDirectory
        this.staticDirectory = staticDirectory
        this.templateDirectory = templateDirectory
        this.outputDirectory = outputDirectory
        this.websiteRoot = websiteRoot
    }

    @Override
    void run() {
        this.createOutputDirectoryIfNotExists()
        this.clearOutputDirectory()
        this.copyStaticContentDirectory()
        this.applyTemplate()
    }

    private createOutputDirectoryIfNotExists() {
        if (Files.notExists this.outputDirectory) {
            Files.createDirectories this.outputDirectory
        }
    }

    private clearOutputDirectory() {
        Files.walkFileTree this.outputDirectory, new DeletingFileVisitor()
    }

    private copyStaticContentDirectory() {
        Files.walkFileTree this.staticDirectory, new CopyingFileVisitor(this.staticDirectory, this.outputDirectory)
    }

    private applyTemplate() {
        final templateEngine = new TemplateEngine(this.websiteRoot, this.dataDirectory, this.templateDirectory, this.outputDirectory)
        templateEngine.run()
    }
}
