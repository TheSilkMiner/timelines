/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating

import groovy.transform.PackageScope

@PackageScope
class FileCache {
    private final Closure<String[]> fileLookup
    private final Map<String, String[]> cache

    FileCache(final Closure<String[]> fileLookup) {
        this.fileLookup = fileLookup
        this.cache = [:]
    }

    def getAt(final String path) {
        cache.computeIfAbsent path, this.fileLookup
    }
}
