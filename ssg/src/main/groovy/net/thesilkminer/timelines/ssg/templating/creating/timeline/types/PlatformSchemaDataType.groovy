/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline.types

import net.thesilkminer.timelines.ssg.templating.TemplateContext

class PlatformSchemaDataType implements TimelineSchemaDataType {

    private static final Map<String, Tuple2<String, Map<String, String>>> VALID_PLATFORMS = [
            '3do' : t('3DO Interactive Multiplayer', 'video_game/3do'),
            'android' : t('Android', 'general/android'),
            'bd' : t('Blu-Ray Disc', 'video/bd'),
            'blackberry' : t('BlackBerry', 'general/blackberry'),
            'cd_audio' : t('CD Audio', 'audio/cd_da'),
            'dvd' : t('DVD', 'video/dvd'),
            'exen' : t('ExEn', 'general/exen'),
            'flash' : t('Adobe Flash Player', 'general/flash'),
            'htc_vive' : t('HTC Vive', 'video_game/htc_vive'),
            'ios' : t('iOS', 'general/apple/ios'),
            'ipados' : t('iPadOS', 'general/apple/ipados'),
            'itv_bell' : t('Bell ExpressVu', 'video_game/itv/bell'),
            'itv_sky_gamestar' : t('Sky Gamestar', 'video_game/itv/sky_gamestar'),
            'j2me' : t('Java for Mobile (J2ME)', 'general/j2me'),
            'luna' : t('Amazon Luna', 'video_game/luna'),
            'ms_pocket_pc' : t('Pocket PC', 'general/ms/ppc', 'orig', 'xp'),
            'ms_win_phone' : t('Microsoft Windows Phone', 'general/ms/win_phone', 'mobile', '8'),
            'ms_xbox' : t('Microsoft Xbox', 'video_game/ms/xbox'),
            'ms_xbox_360' : t('Microsoft Xbox 360', 'video_game/ms/xbox_360'),
            'ms_xbox_one' : t('Microsoft Xbox One', 'video_game/ms/xbox_one'),
            'ms_xbox_xs' : t('Microsoft Xbox Series X/S', 'video_game/ms/xbox_xs'),
            'netflix' : t('Netflix', 'video/netflix'),
            'nintendo_3ds' : t('Nintendo 3DS', 'video_game/nintendo/3ds'),
            'nintendo_64' : t('Nintendo 64', 'video_game/nintendo/64'),
            'nintendo_ds' : t('Nintendo DS', 'video_game/nintendo/ds'),
            'nintendo_gamecube' : t('Nintendo GameCube', 'video_game/nintendo/gamecube'),
            'nintendo_gba' : t('Nintendo GameBoy Advance', 'video_game/nintendo/gba'),
            'nintendo_gbc' : t('Nintendo GameBoy Color', 'video_game/nintendo/gbc'),
            'nintendo_switch' : t('Nintendo Switch', 'video_game/nintendo/switch'),
            'nintendo_wii' : t('Nintendo Wii', 'video_game/nintendo/wii'),
            'nintendo_wii_u' : t('Nintendo Wii U', 'video_game/nintendo/wii_u'),
            'nokia_n_gage' : t('Nokia N-Gage', 'general/nokia_ngage'),
            'oculus' : t('Oculus VR Platforms', 'video_game/oculus'),
            'pc_dos' : t('MS-DOS', 'general/ms/dos'),
            'pc_linux' : t('Linux', 'general/linux'),
            'pc_mac' : t('Apple Macintosh', 'general/apple/mac', 'mac', 'os', 'osx', 'macos'),
            'pc_win' : t('Microsoft Windows', 'general/ms/win', '1', '2', '3', '95', 'xp', 'vista', '8', '10', '11'),
            'prime_video' : t('Amazon Prime Video', 'video/prime_video'),
            'sega_dreamcast' : t('Sega Dreamcast', 'video_game/sega/dreamcast'),
            'sega_saturn' : t('Sega Saturn', 'video_game/sega/saturn'),
            'shockwave' : t('Adobe Shockwave', 'general/shockwave'),
            'sony_playstation' : t('Sony PlayStation', 'video_game/sony/ps'),
            'sony_playstation_2' : t('Sony PlayStation 2', 'video_game/sony/ps2'),
            'sony_playstation_3' : t('Sony PlayStation 3', 'video_game/sony/ps3'),
            'sony_playstation_4' : t('Sony PlayStation 4', 'video_game/sony/ps4'),
            'sony_playstation_5' : t('Sony PlayStation 5', 'video_game/sony/ps5'),
            'sony_playstation_network' : t('Sony Playstation Network', 'video_game/sony/psn', 'globe', 'sphere'),
            'sony_playstation_portable' : t('Sony PlayStation Portable', 'video_game/sony/psp'),
            'sony_playstation_vita' : t('Sony PlayStation Vita', 'video_game/sony/psvita'),
            'stadia' : t('Google Stadia', 'video_game/stadia'),
            'tiger_game_com' : t('Tiger Game.com', 'video_game/game_com'),
            'umd' : t('UMD', 'video/umd'),
            'vhs' : t('VHS', 'video/vhs'),
            'youtube' : t('YouTube', 'video/youtube'),
            'zeebo' : t('Zeebo', 'video_game/zeebo')
    ]

    private final List<String> platforms

    PlatformSchemaDataType(final specifications, final String columnId, final fullData) {
        this.platforms = findPlatforms(columnId, fullData) as List<String>
    }

    private static findPlatforms(final String columnId, final fullData) {
        fullData.collect { it[columnId] }
                .flatten()
                .each {
                    final key = extractKey(it)
                    if (!VALID_PLATFORMS.containsKey(key)) {
                        throw new IllegalArgumentException("Invalid platform ${key} identified in column ${columnId}")
                    }
                    final specifiers = VALID_PLATFORMS[key].v2
                    final subKey = extractSubKey(it)
                    if (!specifiers.containsKey(subKey)) {
                        final messageKey = subKey == null? 'no specifier' : "specifier ${subKey}"
                        throw new IllegalArgumentException("Platform ${key} with ${messageKey} is invalid in column ${columnId}")
                    }
                }
                .collect(PlatformSchemaDataType.&extractKey)
                .toSet()
                .toList()
                .sort { a, b -> VALID_PLATFORMS[a].v1.toLowerCase(Locale.ENGLISH) <=> VALID_PLATFORMS[b].v1.toLowerCase(Locale.ENGLISH) }
    }

    private static t(final String humanName, final String baseImage, final String... specifiers) {
        final Map<String, String> computedSpecifiers = [:]
        if (specifiers.length == 0) {
            computedSpecifiers[null] = makeImage(baseImage, null)
        } else {
            specifiers.each { computedSpecifiers[it] = makeImage(baseImage, it) }
        }
        new Tuple2(humanName, computedSpecifiers)
    }

    private static extractKey(final it) {
        it.takeWhile { it != '{' }
    }

    private static extractSubKey(final it) {
        it.contains('{')? it.takeBetween('{', '}') : null
    }

    private static makeImage(final baseImg, final spec) {
        final builder = new StringBuilder()
        builder << 'img/platform/' << baseImg
        if (spec != null) {
            builder << '--' << spec
        }
        builder << '.svg'
        builder.toString()
    }

    @Override
    void writeHtmlFilter(final String columnId, final StringBuilder builder) {
        builder << '<select id="tl-filter--' << columnId << '" name="' << columnId << '" '
        builder << 'class="tl-js-timeline__filter--input tl-js-timeline__filter--combo-box" '
        builder << 'data-type="combo-box">'
        builder << '<option value="@@all@@" selected="selected">All</option>'
        this.platforms.each {
            final key = extractKey(it)
            builder << '<option value="' << key << '">' << VALID_PLATFORMS[key].v1 << '</option>'
        }
        builder << '</select>'
    }

    @Override
    String computeJsDataString(final specification) {
        specification.join(',')
    }

    @Override
    void renderHumanHtml(final StringBuilder builder, final TemplateContext context, final specification) {
        specification.each { platform ->
            final platformKey = extractKey(platform)
            final subKey = extractSubKey(platform)
            final platformData = VALID_PLATFORMS[platformKey]
            final humanName = platformData.v1
            final imgSpec = platformData.v2[subKey]
            builder << '<span class="tl-timeline__platform" title="' << humanName << '"><img alt="'
            builder << humanName << '" title="' << humanName << '" src="' << context.url(imgSpec)
            builder << '" class="tl-timeline__platform--image" /></span><br />'
        }
    }
}
