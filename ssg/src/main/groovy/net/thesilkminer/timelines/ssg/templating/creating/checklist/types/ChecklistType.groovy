/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist.types

abstract class ChecklistType {

    final String typeName
    final String friendlyName
    final boolean canBeDisabled

    ChecklistType(final String typeName, final String friendlyName, final boolean canBeDisabled) {
        this.typeName = typeName
        this.friendlyName = friendlyName
        this.canBeDisabled = canBeDisabled
    }

    final void render(final StringBuilder builder, final Map data) {
        builder << '<span class="tl-100-checklist__extra tl-js-100-checklist__extra'

        if (this.canBeDisabled) {
            builder << ' tl-js-100-checklist__extra--can-disable" data-enabled="true'
        }

        builder << '" data-extra-type="' << this.typeName << '">'

        this.renderHeading(builder, data)
        this.renderContent(builder, data)
        this.renderFooter(builder, data)

        builder << '</span>'
    }

    abstract void renderHeading(final StringBuilder builder, final Map data)

    abstract void renderContent(final StringBuilder builder, final Map data)

    abstract void renderFooter(final StringBuilder builder, final Map data)

}
