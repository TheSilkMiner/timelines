/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline

import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.TemplateContext
import net.thesilkminer.timelines.ssg.templating.creating.timeline.types.TimelineSchemaDataType
import net.thesilkminer.timelines.ssg.templating.creating.timeline.types.TimelineSchemaDataTypes

class TimelineSchemaColumn {
    final String internalId
    final String name
    final TimelineSchemaDataType type
    final String specifier
    final boolean filterable
    final boolean sortable
    final boolean mandatory

    @SuppressWarnings(['GroovyAssignabilityCheck', 'GrUnresolvedAccess'])
    TimelineSchemaColumn(final String internalId, final idSpec, final fullData) {
        this.internalId = internalId
        this.name = idSpec.column
        this.type = TimelineSchemaDataTypes.of idSpec.type, idSpec, internalId, fullData
        this.specifier = idSpec.specifier
        this.filterable = or(idSpec.filterable, true)
        this.sortable = or(idSpec.sortable, false)
        this.mandatory = or(idSpec.mandatory, false)
    }

    private static or(final a, final b) {
        return a == null? b : a
    }

    @PackageScope
    renderFilterInput(final StringBuilder builder) {
        this.type.writeHtmlFilter(this.internalId, builder)
    }

    @PackageScope
    computeJsDataString(final specification) {
        this.type.computeJsDataString(specification)
    }

    @PackageScope
    renderHumanHtml(final StringBuilder builder, final TemplateContext context, final specification) {
        this.type.renderHumanHtml(builder, context, specification)
    }
}
