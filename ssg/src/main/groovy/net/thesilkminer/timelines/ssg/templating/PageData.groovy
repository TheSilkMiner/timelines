/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating

import groovy.json.JsonSlurper

class PageData {
    private static final Map<FileType, Closure<PageData>> BUILDERS = [
            (FileType.CUSTOM_HTML) : PageData.&fromCustomHtml,
            (FileType.JSON) : PageData.&fromJson
    ]

    final FileType fileType
    final String pageTitle
    final String pageBackground
    final String parentTemplate
    final String additionalCss
    final String additionalJs

    private PageData(
            final FileType fileType,
            final String pageTitle,
            final String pageBackground,
            final String parentTemplate,
            final String additionalSheet,
            final String additionalScript
    ) {
        this.fileType = fileType
        this.pageTitle = pageTitle
        this.pageBackground = pageBackground
        this.parentTemplate = parentTemplate
        this.additionalCss = additionalSheet
        this.additionalJs = additionalScript
    }

    static buildFrom(final FileType fileType, final String[] file) {
        BUILDERS[fileType](file)
    }

    private static fromCustomHtml(final String[] file) {
        new PageData(FileType.CUSTOM_HTML, file[0], file[1], 'html_template', file[2], file[3])
    }

    private static fromJson(final String[] file) {
        final jsonReader = new JsonSlurper()
        final json = jsonReader.parseText(file.join('\n'))
        //noinspection GroovyAssignabilityCheck, GrUnresolvedAccess
        new PageData(
                FileType.JSON,
                json.page_data.title,
                json.page_data.background,
                json.page_data.parent_template,
                json.page_data.additional_css,
                json.page_data.additional_js
        )
    }
}
