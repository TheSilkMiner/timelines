/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline.types

import net.thesilkminer.timelines.ssg.templating.TemplateContext

class EnumerationSchemaDataType implements TimelineSchemaDataType {
    private Map<String, String> values

    EnumerationSchemaDataType(final specifications, final String columnId, final fullData) {
        this.values = validating(specifications.valid_values, columnId, fullData)
    }

    private static Map<String, String> validating(final validValues, final String columnId, final fullData) {
        fullData.each {
            final value = it[columnId]
            if (!validValues.containsKey(value)) {
                throw new IllegalArgumentException("Illegal enumeration type ${it} for column ${columnId}")
            }
        }
        validValues.toSorted { e1, e2 -> e1.value.toLowerCase(Locale.ENGLISH) <=> e2.value.toLowerCase(Locale.ENGLISH) }
    }

    @Override
    void writeHtmlFilter(final String columnId, final StringBuilder builder) {
        builder << '<select id="tl-filter--' << columnId << '" name="' << columnId << '" '
        builder << 'class="tl-js-timeline__filter--input tl-js-timeline__filter--combo-box" '
        builder << 'data-type="combo-box">'
        builder << '<option value="@@all@@" selected="selected">All</option>'
        this.values.each { k, v ->
            builder << '<option value="' << k << '">' << (v.isEmpty()? '(None)' : v) << '</option>'
        }
        builder << '</select>'
    }

    @Override
    String computeJsDataString(final specification) {
        specification
    }

    @Override
    void renderHumanHtml(final StringBuilder builder, final TemplateContext context, final specification) {
        builder << this.values[specification.toString()]
    }
}
