/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist

import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.creating.checklist.types.ChecklistType

@PackageScope
class ChecklistContentRenderer {
    final Map<String, ChecklistType> extraTypes
    final int headingLevel

    ChecklistContentRenderer(final Map<String, ChecklistType> extraTypes, final int headingLevel = 2) {
        this.extraTypes = extraTypes
        this.headingLevel = headingLevel
    }

    def renderContents(final StringBuilder builder, final contents) {
        this.renderContentList(builder, contents)
    }

    private renderContentList(final StringBuilder builder, final List contents) {
        final hasSubsections = contents.any { it instanceof Map }
        final hasMulti = contents.any { it instanceof Integer }
        final shouldRenderList = !hasSubsections && !hasMulti

        if (shouldRenderList) {
            builder << '<span class="tl-100-checklist__list" data-list-type="single">'
        }

        contents.each { this.render(builder, it) }

        if (shouldRenderList) {
            builder << '</span>'
        }
    }

    private render(final StringBuilder builder, final Map section) {
        builder << '<a id="tl-100-checklist--anchor--' << section.id.toLowerCase(Locale.ENGLISH).replace([ ' ' : '' ])
        builder << '"></a><h' << this.headingLevel << '>'
        builder << section.id
        builder << '</h' << this.headingLevel << '>'
        if (section.free_text) {
            builder << '<p>' << section.free_text.join(' ') << '</p>'
        }
        final renderer = new ChecklistContentRenderer(this.extraTypes, this.headingLevel + 1)
        renderer.renderContents(builder, section.contents)
    }

    private render(final StringBuilder builder, final List elementAndExtras) {
        final element = elementAndExtras[0]
        final extras = elementAndExtras.drop(1)

        this.renderEntry(builder) {
            this.renderLeaf(builder, element)
            extras.each { this.renderExtra(builder, it) }
        }
    }

    private render(final StringBuilder builder, final String entryName) {
        this.renderEntry(builder) { this.renderLeaf(builder, entryName) }
    }

    private render(final StringBuilder builder, final int entryQuantity) {
        this.renderLeaf(builder, entryQuantity)
    }

    private renderLeaf(final StringBuilder builder, final String entryName) {
        builder << '<span class="tl-100-checklist__entry--name">' << entryName << '</span>'
    }

    private renderLeaf(final StringBuilder builder, final int entryQuantity) {
        builder << '<span class="tl-100-checklist__list" data-list-type="multi">'
        for (int i = 0; i < entryQuantity; ++i) {
            builder << '<span class="tl-100-checklist__entry'
            if (i % 10 == 9) {
                builder << ' tl-100-checklist__multi--space-request'
            }
            builder << '"><input type="checkbox" /></span>'
        }
        builder << '</span>'
    }

    private renderEntry(final StringBuilder builder, final Closure block) {
        builder << '<span class="tl-100-checklist__list-entry"><input type="checkbox" /><span class="tl-100-checklist'
        builder << '__entry">'
        block()
        builder << '</span></span>'
    }

    private renderExtra(final StringBuilder builder, final Map extra) {
        final String type = extra.type
        final ChecklistType checklistType = this.extraTypes[type]
        if (!checklistType) {
            throw new IllegalArgumentException("No valid extra with type '$type' exists, neither builtin or custom")
        }
        checklistType.render(builder, extra)
    }
}
