/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating

import groovy.json.JsonSlurper
import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext
import net.thesilkminer.timelines.ssg.templating.creating.checklist.ChecklistParser
import net.thesilkminer.timelines.ssg.templating.creating.timeline.TimelineParser
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderReplacer

@PackageScope
class JsonCreator extends SimpleFileCreator {
    private static final Map<String, Closure<?>> PARSERS = [
            '100_checklist' : ChecklistParser.&makeContents,
            'timeline' : TimelineParser.&makeContents
    ]

    @Override
    void makeContents(final StringBuilder builder, final PlaceholderReplacer replacer, final TemplateContext context, final String[] template, final String[] file, final PageData data) {
        final jsonReader = new JsonSlurper()
        final json = jsonReader.parseText(file.join('\n'))
        findParser(json)(builder, context, template, data, json)
    }

    @SuppressWarnings(['GroovyAssignabilityCheck', 'GrUnresolvedAccess'])
    static findParser(final json) {
        PARSERS[json.schema_type]
    }
}
