/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist.types

class ChecklistTypes {
    private static final Map<String, Closure<? extends ChecklistType>> DEFAULT_TYPE_CREATORS = [
            'info' : { new InfoChecklistType('info', 'Information', true) },
            'warn' : { new WarningChecklistType('warn', 'Warnings', false) }
    ]

    static createTypes(final extra) {
        final Map<String, ChecklistType> types = [:]

        DEFAULT_TYPE_CREATORS.each { key, value -> types[key] = value() }
        extra.each { name, spec -> types[name] = CustomChecklistType.of(name, spec) { types } }

        types
    }
}
