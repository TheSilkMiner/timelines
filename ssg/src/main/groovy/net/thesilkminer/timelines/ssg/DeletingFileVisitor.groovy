/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg

import groovy.transform.PackageScope

import java.nio.file.FileVisitResult
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.SimpleFileVisitor
import java.nio.file.attribute.BasicFileAttributes

@PackageScope
class DeletingFileVisitor extends SimpleFileVisitor<Path> {

    @Override
    FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
        Objects.requireNonNull file
        Objects.requireNonNull attrs
        Files.delete file
        super.visitFile file, attrs
    }

    @Override
    FileVisitResult postVisitDirectory(final Path dir, final IOException exc) {
        Objects.requireNonNull dir
        if (exc != null) {
            throw exc
        }
        Files.delete dir
        super.postVisitDirectory dir, exc
    }
}
