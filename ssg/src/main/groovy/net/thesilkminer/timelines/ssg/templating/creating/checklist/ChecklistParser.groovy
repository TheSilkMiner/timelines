/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist

import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderCommands
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderReplacer

class ChecklistParser {

    private static final Map<String, Closure<?>> JSON_CUSTOM_COMMANDS = [
            'checklist' : ChecklistParser.&checklist,
            'extra-checkboxes' : ChecklistParser.&extraCheckboxes,
            'heading' : ChecklistParser.&heading,
            'preamble' : ChecklistParser.&preamble,
            'toc' : ChecklistParser.&toc
    ]

    static void makeContents(
            final StringBuilder builder,
            final TemplateContext context,
            final String[] template,
            final PageData data,
            final json
    ) {
        final jsonData = ChecklistDataHolder.of(json)
        final subReplacer = new PlaceholderReplacer(PlaceholderCommands.ALL + specializeCommandsFor(jsonData))
        template.each {
            subReplacer.replacingPlaceholders(builder, it, context, data) {
                throw new IllegalStateException('Invalid {{ page-content }} command found in JSON template: JSON pages have no content')
            }
        }
    }

    private static Map<String, Closure<?>> specializeCommandsFor(final jsonData) {
        JSON_CUSTOM_COMMANDS.collectEntries([:]) { command, executor ->
            [ command, { builder, context, data, arguments -> executor(builder, context, data, jsonData, arguments) } ]
        }
    }

    private static void checklist(final StringBuilder builder, final TemplateContext context, final PageData data, final ChecklistDataHolder jsonData, final String[] arguments) {
        jsonData.renderContents(builder)
    }

    private static void extraCheckboxes(final StringBuilder builder, final TemplateContext context, final PageData data, final ChecklistDataHolder jsonData, final String[] arguments) {
        jsonData.renderFilters(builder)
    }

    private static void heading(final StringBuilder builder, final TemplateContext context, final PageData data, final ChecklistDataHolder jsonData, final String[] arguments) {
        jsonData.renderHeading(builder)
    }

    private static void preamble(final StringBuilder builder, final TemplateContext context, final PageData data, final ChecklistDataHolder jsonData, final String[] arguments) {
        jsonData.renderPreamble(builder)
    }

    private static void toc(final StringBuilder builder, final TemplateContext context, final PageData data, final ChecklistDataHolder jsonData, final String[] arguments) {
        jsonData.renderTableOfContents(builder)
    }

}
