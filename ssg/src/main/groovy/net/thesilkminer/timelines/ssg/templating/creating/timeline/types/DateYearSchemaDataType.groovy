/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline.types

import net.thesilkminer.timelines.ssg.templating.TemplateContext

class DateYearSchemaDataType implements TimelineSchemaDataType {

    private List<Integer> years
    private boolean requiresAdBc

    DateYearSchemaDataType(final specifications, final String columnId, final fullData) {
        this.years = findYears(columnId, fullData)
        this.requiresAdBc = this.years.any { it < 0 }
    }

    @SuppressWarnings('GrUnresolvedAccess')
    private static findYears(final String columnId, final fullData) {
        fullData.collect { it[columnId].toInteger() } as List<Integer>
    }

    @Override
    void writeHtmlFilter(final String columnId, final StringBuilder builder) {
        builder << '<select id="tl-filter--' << columnId << '" name="' << columnId << '" '
        builder << 'class="tl-js-timeline__filter--input tl-js-timeline__filter--combo-box" '
        builder << 'data-type="combo-box">'
        builder << '<option value="" selected="selected">All</option>'
        this.years.each {
            builder << '<option value="' << it << '">'

            if (this.requiresAdBc) {
                final absoluteYear = it.abs()
                final adBc = it < 0? ' BC' : ' AD'
                builder << absoluteYear << adBc
            } else {
                builder << it
            }

            builder << '</option>'
        }
        builder << '</select>'
    }

    @Override
    String computeJsDataString(final specification) {
        specification.toString()
    }

    @Override
    void renderHumanHtml(final StringBuilder builder, final TemplateContext context, final specification) {
        final spec = specification.toInteger()

        if (this.requiresAdBc) {
            final absoluteYear = spec.abs()
            final adBc = spec < 0? ' BC' : ' AD'
            builder << absoluteYear << adBc
        } else {
            builder << spec
        }
    }
}
