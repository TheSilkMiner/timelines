/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline.types

import net.thesilkminer.timelines.ssg.templating.TemplateContext

class TypeSchemaDataType implements TimelineSchemaDataType {

    private static final Map<String, String> VALID_TYPES = [
            'board_game' : 'Board Game',
            'comic' : 'Comic',
            'documentary' : 'Documentary',
            'gambling_game' : 'Casino Game',
            'movie' : 'Movie',
            'novel' : 'Novel',
            'play' : 'Play',
            'radio_play' : 'Radio Play',
            'short' : 'Short Movie',
            'tv_series' : 'TV Series',
            'video_game' : 'Video Game'
    ]

    private final List<String> knownTypes

    TypeSchemaDataType(final specifications, final String columnId, final fullData) {
        this.knownTypes = computeTypesFrom(columnId, fullData)
    }

    private static computeTypesFrom(final String columnId, final fullData) {
        fullData.collect { it[columnId].toString() }
                .toSet()
                .toList()
                .each {
                    if (!VALID_TYPES.containsKey(it)) {
                        throw new IllegalArgumentException("Invalid type ${it} identified in column ${columnId}")
                    }
                }
                .sort {a, b -> VALID_TYPES[a].toLowerCase(Locale.ENGLISH) <=> VALID_TYPES[b].toLowerCase(Locale.ENGLISH)  }
    }

    @Override
    void writeHtmlFilter(final String columnId, final StringBuilder builder) {
        builder << '<select id="tl-filter--' << columnId << '" name="' << columnId << '" '
        builder << 'class="tl-js-timeline__filter--input tl-js-timeline__filter--combo-box" '
        builder << 'data-type="combo-box">'
        builder << '<option value="@@all@@" selected="selected">All</option>'
        this.knownTypes.each {
            builder << '<option value="' << it << '">' << VALID_TYPES[it] << '</option>'
        }
        builder << '</select>'
    }

    @Override
    String computeJsDataString(final specification) {
        specification
    }

    @Override
    void renderHumanHtml(final StringBuilder builder, final TemplateContext context, final specification) {
        builder << VALID_TYPES[specification.toString()]
    }
}
