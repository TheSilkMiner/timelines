/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.placeholder

import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext

class PlaceholderReplacer {
    private static final PLACEHOLDER_BEGIN = /\{\{/
    private static final PLACEHOLDER_END = /}}/

    private static final INCLUSION_HANDLER_COMMAND = 'page-content'

    final Map<String, Closure<?>> commands

    PlaceholderReplacer(final Map<String, Closure<?>> commands) {
        this.commands = commands
    }

    void replacingPlaceholders(final StringBuilder builder, final String line, final TemplateContext context, final PageData data, final Closure<Void> inclusionHandler) {
        final components = splitIntoComponents line

        components.eachWithIndex{ token, index ->
            if ((index & 1) != 0) {
                this.replacePlaceholder(builder, token, context, data, inclusionHandler)
            } else {
                builder << token.trim()
            }
        }
    }

    private static splitIntoComponents(final String line) {
        line.split(PLACEHOLDER_BEGIN)*.split(PLACEHOLDER_END).flatten() as List<String>
    }

    private void replacePlaceholder(final StringBuilder builder, final String token, final TemplateContext context, final PageData data, final Closure<Void> inclusionHandler) {
        final parseResults = parseCommand(token)

        if (INCLUSION_HANDLER_COMMAND == parseResults.command) {
            inclusionHandler(builder)
        } else {
            final commandHandler = this.commands[parseResults.command]
            try {
                if (commandHandler == null) {
                    throw new NullPointerException("Unknown command ${parseResults.command}")
                }
                commandHandler(builder, context, data, parseResults.arguments)
            } catch (e) {
                throw new IllegalArgumentException("Unable to run command '${parseResults.command}'", e)
            }
        }
    }

    private static parseCommand(final String token) {
        final splitResults = token.trim().split(/ /)
        return [ command : splitResults[0], arguments: splitResults.drop(1) ]
    }
}
