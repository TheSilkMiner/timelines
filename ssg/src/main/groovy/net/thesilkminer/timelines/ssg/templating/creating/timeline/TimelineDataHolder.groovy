/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline

import net.thesilkminer.timelines.ssg.templating.TemplateContext

class TimelineDataHolder {
    private final String heading
    private final String[] preamble
    private final TimelineSchema schema
    private final data

    private TimelineDataHolder(final String heading, final String[] preamble, final TimelineSchema schema, final data) {
        this.heading = heading
        this.preamble = preamble
        this.schema = schema
        this.data = data
    }

    static of(final json) {
        final String heading = json.heading
        final String[] preamble = json.preamble
        final data = json.data
        final TimelineSchema schema = TimelineSchema.of json.schema, data
        new TimelineDataHolder(heading, preamble, schema, data)
    }

    def renderHeading(final StringBuilder builder) {
        builder << this.heading
    }

    def renderPreamble(final StringBuilder builder) {
        builder << this.preamble.join(' ')
    }

    def renderSorters(final StringBuilder builder) {
        builder << '<div class="tl-timeline__filter tl-js-timeline__sorter">'
        builder << '<label class="tl-timeline__filter--name" for="tl-sorter">Sort by:</label>'
        builder << '<select id="tl-sorter" name="sort_by" class="tl-js-timeline__sorter--input">'
        builder << '<option value="no" selected="selected">#</option>'
        this.schema.allColumns()
            .findAll { it.sortable }
            .each { builder << '<option value="' << it.internalId << '">' << it.name << '</option>' }
        builder << '</select>'
        builder << '</div>'
    }

    def renderFilters(final StringBuilder builder) {
        this.schema.allColumns()
                .findAll { it.filterable }
                .each { renderFilter(builder, it) }
    }

    def renderColSpecs(final StringBuilder builder) {
        builder << '<col class="tl-timeline__col tl-timeline__entry-number tl-timeline__entry--mandatory" />'
        this.schema.allColumns().each {
            builder << '<col class="tl-timeline__col'
            if (it.specifier != null) {
                builder << ' tl-timeline__entry-' << it.specifier
            }
            if (it.mandatory) {
                builder << ' tl-timeline__entry--mandatory'
            }
            builder << '" />'
        }
    }

    def renderHeaders(final StringBuilder builder) {
        builder << '<th class="tl-timeline__cell tl-timeline__cell--header tl-timeline__cell--even tl-timeline__entry--mandatory" data-id="no">#</th>'
        this.schema.allColumns().eachWithIndex { column, index ->
            final actualIndex = index + 1 // Column 0 is the number column
            final evenOdd = (actualIndex % 2) == 0? 'even' : 'odd'
            builder << '<th class="tl-timeline__cell tl-timeline__cell--header tl-timeline__cell--' << evenOdd
            if (column.specifier != null) {
                builder << ' tl-timeline__entry-' << column.specifier
            }
            if (column.mandatory) {
                builder << ' tl-timeline__entry--mandatory'
            }
            builder << '" data-id="'
            builder << column.internalId << '">' << column.name << '</th>'
        }
    }

    def renderEntries(final StringBuilder builder, final TemplateContext context) {
        this.data.eachWithIndex { entry, int index ->
            renderEntry(builder, index + 1, entry, context, this.schema)
        }
    }

    private static renderFilter(final StringBuilder builder, final TimelineSchemaColumn column) {
        builder << '<div class="tl-timeline__filter tl-js-timeline__filter" data-target="' << column.internalId << '">'
        builder << '<label class="tl-timeline__filter--name" for="tl-filter--' << column.internalId << '">' << column.name << '</label>'

        column.renderFilterInput(builder)

        builder << '</div>'
    }

    private static renderEntry(final StringBuilder builder, final int oneBasedIndex, final entrySpec, final TemplateContext context, final TimelineSchema schema) {
        entrySpec.each { String columnName, _ ->
            if (schema[columnName] == null) {
                throw new IllegalArgumentException("No such column '$columnName' defined in schema")
            }
        }

        builder << '<tr class="tl-timeline__entry tl-js-timeline__entry" data-no="' << oneBasedIndex << '" '
        entrySpec.each { String columnName, columnSpec ->
            builder << 'data-' << columnName << '="' << schema[columnName].computeJsDataString(columnSpec) << '" '
        }
        builder << '>'
        builder << '<td class="tl-timeline__cell tl-timeline__cell--entry tl-timeline__cell--even tl-timeline__entry--mandatory">' << oneBasedIndex << '</td>'
        entrySpec.eachWithIndex { entry, int index ->
            final actualIndex = index + 1 // Column 0 is the number column
            final evenOdd = (actualIndex % 2) == 0? 'even' : 'odd'
            final key = entry.key.toString()
            final column = schema[key]

            builder << '<td class="tl-timeline__cell tl-timeline__cell--entry tl-timeline__cell--' << evenOdd
            if (column.specifier != null) {
                builder << ' tl-timeline__entry-' << column.specifier
            }
            if (column.mandatory) {
                builder << ' tl-timeline__entry--mandatory'
            }
            builder << '">'

            column.renderHumanHtml(builder, context, entry.value)

            builder << '</td>'
        }
        builder << '</tr>'
    }
}
