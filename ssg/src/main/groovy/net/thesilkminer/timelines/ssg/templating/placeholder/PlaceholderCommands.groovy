/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.placeholder

import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext

class PlaceholderCommands {
    static final Map<String, Closure<?>> ALL = [
            'background': closureTP(PlaceholderCommands.&background),
            'custom-css': closureTP(PlaceholderCommands.&customCss),
            'custom-js': closureTP(PlaceholderCommands.&customJs),
            'title' : closureP(PlaceholderCommands.&title),
            'todo': closure(PlaceholderCommands.&todo),
            'url' : closureTS(PlaceholderCommands.&url)
    ]

    private static void background(final StringBuilder builder, final TemplateContext context, final PageData data) {
        builder << (data.pageBackground.startsWith("http")? data.pageBackground : context.url("img/background/${data.pageBackground}"))
    }

    private static void customCss(final StringBuilder builder, final TemplateContext context, final PageData data) {
        builder << (data.additionalCss.isEmpty()? '' : "<link href=\"${context.url "css/${data.additionalCss}.css"}\" rel=\"stylesheet\" />")
    }

    private static void customJs(final StringBuilder builder, final TemplateContext context, final PageData data) {
        builder << (data.additionalJs.isEmpty()? '' : "<script src=\"${context.url "js/${data.additionalJs}.js"}\" defer=\"defer\"></script>")
    }

    private static void title(final StringBuilder builder, final PageData data) {
        builder << data.pageTitle
    }

    private static void todo(final StringBuilder builder) {
        builder << 'Work in Progress'
    }

    private static void url(final StringBuilder builder, final TemplateContext context, final String[] arguments) {
        builder << context.url(arguments)
    }

    //region Closure Helpers

    private static Closure<?> closureTS(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, context, arguments) }
    }

    private static Closure<?> closurePS(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, data, arguments) }
    }

    private static Closure<?> closureTP(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, context, data) }
    }

    private static Closure<?> closureT(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, context) }
    }

    private static Closure<?> closureP(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, data) }
    }

    private static Closure<?> closureS(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder, arguments) }
    }

    private static Closure<?> closure(final Closure<?> child) {
        return { builder, context, data, arguments -> child(builder) }
    }

    //endregion
}
