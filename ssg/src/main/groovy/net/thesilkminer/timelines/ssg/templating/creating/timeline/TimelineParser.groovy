/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.timeline

import net.thesilkminer.timelines.ssg.templating.PageData
import net.thesilkminer.timelines.ssg.templating.TemplateContext
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderCommands
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderReplacer

class TimelineParser {
    private static final Map<String, Closure<?>> JSON_CUSTOM_COMMANDS = [
            'col-specifications' : TimelineParser.&colSpecifications,
            'entries' : TimelineParser.&entries,
            'filters' : TimelineParser.&filters,
            'headers' : TimelineParser.&headers,
            'heading' : TimelineParser.&heading,
            'preamble' : TimelineParser.&preamble,
            'sorters' : TimelineParser.&sorters
    ]

    static void makeContents(
            final StringBuilder builder,
            final TemplateContext context,
            final String[] template,
            final PageData data,
            final json
    ) {
        final jsonData = TimelineDataHolder.of json
        final subReplacer = new PlaceholderReplacer(PlaceholderCommands.ALL + specializeCommandsFor(jsonData))
        template.each {
            subReplacer.replacingPlaceholders(builder, it, context, data) {
                throw new IllegalStateException('Invalid {{ page-content }} command found in JSON template: JSON pages have no content')
            }
        }
    }

    private static Map<String, Closure<?>> specializeCommandsFor(final jsonData) {
        JSON_CUSTOM_COMMANDS.collectEntries([:]) { command, executor ->
            [ command, { builder, context, data, arguments -> executor(builder, context, data, jsonData, arguments) } ]
        }
    }

    private static void colSpecifications(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderColSpecs(builder)
    }

    private static void entries(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderEntries(builder, context)
    }

    private static void filters(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderFilters(builder)
    }

    private static void headers(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderHeaders(builder)
    }

    private static void heading(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderHeading(builder)
    }

    private static void preamble(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderPreamble(builder)
    }

    private static void sorters(final StringBuilder builder, final TemplateContext context, final PageData data, final TimelineDataHolder jsonData, final String[] arguments) {
        jsonData.renderSorters(builder)
    }
}
