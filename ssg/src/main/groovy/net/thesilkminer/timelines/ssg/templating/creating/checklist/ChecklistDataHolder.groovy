/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist

import groovy.transform.PackageScope
import net.thesilkminer.timelines.ssg.templating.creating.checklist.types.ChecklistType
import net.thesilkminer.timelines.ssg.templating.creating.checklist.types.ChecklistTypes

class ChecklistDataHolder {

    final String heading
    final String[] preamble
    final Map<String, ChecklistType> types
    final contents

    private ChecklistDataHolder(final String heading, final String[] preamble, final Map<String, ChecklistType> types, final contents) {
        this.heading = heading
        this.preamble = preamble
        this.types = types
        this.contents = contents
    }

    static of(final json) {
        final String heading = json.heading
        final String[] preamble = json.preamble
        final Map<String, ChecklistType> types = ChecklistTypes.createTypes(json.extra_types)
        final contents = json.checklist
        new ChecklistDataHolder(heading, preamble, types, contents)
    }

    @PackageScope
    renderHeading(final StringBuilder builder) {
        builder << this.heading
    }

    @PackageScope
    renderPreamble(final StringBuilder builder) {
        builder << this.preamble.join(' ')
    }

    @PackageScope
    renderTableOfContents(final StringBuilder builder) {
        final renderer = new ChecklistTocRenderer()
        renderer.renderContents(builder, this.contents)
    }

    @PackageScope
    renderFilters(final StringBuilder builder) {
        this.types.values()
                .findAll { it.canBeDisabled }
                .each {
                    builder << '<span class="tl-js-100-checklist__extra-checkbox" data-extra-type="' << it.typeName
                    builder << '"><input type="checkbox" checked="checked" /> ' << it.friendlyName << '</span>'
                }
    }

    @PackageScope
    renderContents(final StringBuilder builder) {
        final renderer = new ChecklistContentRenderer(this.types)
        renderer.renderContents(builder, this.contents)
    }

}
