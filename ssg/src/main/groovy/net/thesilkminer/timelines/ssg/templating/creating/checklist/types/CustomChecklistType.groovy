/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating.creating.checklist.types

import groovy.transform.PackageScope

@PackageScope
class CustomChecklistType extends ChecklistType {

    // TODO("Make all of this better")
    private static final class Formatter {
        private final String pre
        private final String split
        private final String post
        private final Map<String, String> helpText

        Formatter(final String format, final Map<String, String> helpText) {
            final brace = format.indexOf('{')
            if (brace == -1) {
                throw new IllegalArgumentException("Not a format string '$format'")
            }

            final def (pre, post) = format.split(/\{/, 2)
            final otherBrace = post.indexOf('}')
            if (otherBrace == -1) {
                throw new IllegalArgumentException("Not a format string '$format'")
            }

            final def (mid, end) = post.split(/}/, 2)
            if (mid.length() > 1) {
                throw new IllegalArgumentException("Not a format string '$format'")
            }

            this.pre = pre
            this.split = mid.length() == 0? null : mid
            this.post = end
            this.helpText = helpText
        }

        String format(final String content) {
            if (this.split != null) {
                throw new IllegalArgumentException("Content must be an array")
            }

            "${this.pre}${this.replaceTextWithHelp(content)}${this.post}"
        }

        String format(final List<String> contents) {
            if (this.split == null) {
                throw new IllegalArgumentException("Content must not be an array")
            }

            "${this.pre}${this.replaceTextWithHelp(contents.join("${this.split} "))}${this.post}"
        }

        private String replaceTextWithHelp(final String data) {
            this.helpText.inject(data, Formatter.&replace)
        }

        private static String replace(final String initial, final Map.Entry<String, String> data) {
            initial.replaceAll(/\b${data.key}\b/, $/<abbr title="${data.value}">${data.key}</abbr>/$)
        }
    }

    private final Closure<ChecklistType> parent
    private final Formatter format

    CustomChecklistType(
            final String typeName,
            final String friendlyName,
            final boolean canBeDisabled,
            final Closure<ChecklistType> parent,
            final String format,
            final Map<String, String> helpText
    ) {
        super(typeName, friendlyName, canBeDisabled)
        this.parent = parent.memoize()
        this.format = new Formatter(format, helpText)
    }

    static of(final String name, final Map spec, final Closure<Map<String, ChecklistType>> types) {
        final Closure<ChecklistType> parent = { types()[spec.parent] }
        final String friendlyName = spec.friendly_name
        final boolean canBeDisabled = spec.can_disable
        final String format = spec.format
        final Map<String, String> helpText = spec.help_text as Map<String, String> ?: [:]
        new CustomChecklistType(name, friendlyName, canBeDisabled, parent, format, helpText)
    }

    @Override
    void renderHeading(final StringBuilder builder, final Map data) {
        builder << '<span class="tl-100-checklist__extra--ask-type-render" data-ask="' << this.parent().typeName
        builder << '">'
        this.parent().renderHeading(builder, data)
    }

    @Override
    void renderContent(final StringBuilder builder, final Map data) {
        if (data.contents) {
            builder << this.format.format(data.contents)
        } else {
            builder << this.format.format(data.content)
        }
    }

    @Override
    void renderFooter(final StringBuilder builder, final Map data) {
        this.parent().renderFooter(builder, data)
        builder << '</span>'
    }
}
