/*
 * Static Site Generator customized for the 'Timelines' project
 * Copyright (C) 2021 TheSilkMiner <thesilkminer <at> outlook <dot> com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.thesilkminer.timelines.ssg.templating

import net.thesilkminer.timelines.ssg.templating.creating.FileCreators
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderCommands
import net.thesilkminer.timelines.ssg.templating.placeholder.PlaceholderReplacer

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class TemplateEngine {
    private static final TEMPLATE_FILE_EXTENSION = '.tmp'
    private static final PAGE_LAYOUT_MAIN_FILE = "page_layout$TEMPLATE_FILE_EXTENSION"

    private final Path dataDirectory
    private final Path templateDirectory
    private final Path outputDirectory
    private final TemplateContext context
    private final PlaceholderReplacer replacer

    TemplateEngine(
            final String siteRoot,
            final Path dataDirectory,
            final Path templateDirectory,
            final Path outputDirectory
    ) {
        this.dataDirectory = dataDirectory
        this.templateDirectory = templateDirectory
        this.outputDirectory = outputDirectory
        this.context = new TemplateContext(siteRoot, new FileCache(this.&findFile))
        this.replacer = new PlaceholderReplacer(PlaceholderCommands.ALL)
    }

    def run() {
        Files.walkFileTree this.dataDirectory, new TemplateVisitor(this.dataDirectory, this.&runOnDirectory, this.&runOnFile)
    }

    private runOnDirectory(final Path directory) {
        final output = this.outputDirectory.resolve directory
        if (!Files.exists(output)) {
            Files.createDirectory output
        }
    }

    private runOnFile(final Path file) {
        try {
            final pageLayoutTemplate = this.context.lookupFile(PAGE_LAYOUT_MAIN_FILE)
            final targetContents = this.context.lookupFile(file.toString())
            final fileType = findFileTypeFrom file
            final pageData = PageData.buildFrom fileType, targetContents
            final parentTemplate = this.context.lookupFile("${pageData.parentTemplate}$TEMPLATE_FILE_EXTENSION")
            final output = this.outputDirectory.resolve findOutputFileNameFor(file, fileType)

            println("Templating file '${file.toAbsolutePath()}', output into '${output.toAbsolutePath()}'")

            this.runTemplatingJob(pageLayoutTemplate, parentTemplate, targetContents, pageData, output)
        } catch (final Exception e) {
            throw new RuntimeException("An error occurred while trying to run templating job for file $file", e)
        }
    }

    private static findFileTypeFrom(final file) {
        FileType.from file.fileName.toString()
    }

    private static findOutputFileNameFor(final file, final type) {
        file.toString().replace type.suffix, '.html'
    }

    private runTemplatingJob(final pageLayout, final parent, final file, final data, final output) {
        Files.writeString output, this.createFile(pageLayout, parent, file, data)
    }

    private createFile(final String[] pageLayout, final String[] parent, final String[] file, final PageData data) {
        final creator = FileCreators.find data.fileType
        final builder = new StringBuilder()
        creator.createFile this.replacer, this.context, pageLayout, parent, file, data, builder
        builder.toString()
    }

    private String[] findFile(final String path) {
        final targetPath = Paths.get path
        final targetDirectory = path.endsWith(TEMPLATE_FILE_EXTENSION)? this.templateDirectory : this.dataDirectory
        Files.readAllLines targetDirectory.resolve(targetPath)
    }
}
