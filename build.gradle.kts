plugins {
    id("net.thesilkminer.timelines.website-plugin")
}

repositories {}

dependencies {}

tasks {
    withType<Wrapper> {
        gradleVersion = "7.4"
        distributionType = Wrapper.DistributionType.ALL
    }
}
