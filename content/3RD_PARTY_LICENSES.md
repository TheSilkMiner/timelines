# 3rd Party Licenses

This document covers the licenses for the 3rd-party content used in this subproject.
This content is not covered by the default project license.

Licenses are shown in the following table, with the file path, clickable license name
(leading to the license text), and attribution if necessary. Any file that is not listed
in the table is assumed to be covered by the project license.

A comment inside the file is used to mention any kinds of alterations that may have been
made to the original contents. A common example of such alterations is *minification*
(i.e. optimizing the file for space).

| File Path | License | Attribution |
| --------- | ------- | ----------- |
| `static/img/background/timelines/biohazard.png` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) + All Rights Reserved (Fair Use of) | Resident Evil Fandom (Users: [Akaza](https://residentevil.fandom.com/wiki/User:Akaza), [Forerunner](https://residentevil.fandom.com/wiki/User:Forerunner)); Capcom Co., Ltd. |
| `static/img/background/timelines/tomb_raider.png` | Public Domain + [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Tomb Raider Fandom (Users: [Blackpill](https://tombraider.fandom.com/wiki/User:Blackpill), [KillerZ](https://tombraider.fandom.com/wiki/User:KillerZ)) |
| `static/img/platform/audio/cd_da.svg` | Public Domain | - |
| `static/img/platform/general/android.svg` | Public Domain | - |
| `static/img/platform/general/apple/ios.svg` | Public Domain | - |
| `static/img/platform/general/apple/ipados.svg` | Public Domain | - |
| `static/img/platform/general/apple/mac--mac.svg` | Public Domain | - |
| `static/img/platform/general/apple/mac--macos.svg` | Public Domain | - |
| `static/img/platform/general/apple/mac--os.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [TheRobloxGuy9009](https://logos.fandom.com/wiki/User:TheRobloxGuy9009)) (Original) |
| `static/img/platform/general/apple/mac--osx.svg` | Public Domain | - |
| `static/img/platform/general/blackberry.svg` | Public Domain | - |
| `static/img/platform/general/exen.svg` | All Rights Reserved (Fair Use of) (Embed) | In-Fusio |
| `static/img/platform/general/flash.svg` | Public Domain | - |
| `static/img/platform/general/j2me.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [AdventureStod](https://logos.fandom.com/wiki/User:AdventureStod)) (Original) |
| `static/img/platform/general/linux.svg` | [CC-0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.en) | - |
| `static/img/platform/general/ms/dos.svg` | [MIT](legal/mit.ms_dos.md) | Microsoft Corporation |
| `static/img/platform/general/ms/ppc--orig.svg` | Public Domain | - |
| `static/img/platform/general/ms/ppc--xp.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [Claytonc3008](https://logos.fandom.com/wiki/User:Claytonc3008)) |
| `static/img/platform/general/ms/win--8.svg` | Public Domain | - |
| `static/img/platform/general/ms/win--10.svg` | Public Domain | - |
| `static/img/platform/general/ms/win--11.svg` | Public Domain | - |
| `static/img/platform/general/ms/win--95.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [Tjdrum2000](https://logos.fandom.com/wiki/User:Tjdrum2000)) (Original) |
| `static/img/platform/general/ms/win--vista.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [Tjdrum2000](https://logos.fandom.com/wiki/User:Tjdrum2000)) (Original) |
| `static/img/platform/general/ms/win--xp.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [SkylerMFN](https://logos.fandom.com/wiki/User:SkylerMFN)) (Original) |
| `static/img/platform/general/ms/win_phone--8.svg` | Public Domain | - |
| `static/img/platform/general/ms/win_phone--mobile.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) (Embed) | Logopedia (User: [Asocialreject](https://logos.fandom.com/wiki/User:Asocialreject)) (Original) |
| `static/img/platform/general/nokia_ngage.svg` | Public Domain | - |
| `static/img/platform/general/shockwave.svg` | Public Domain | - |
| `static/img/platform/video/bd.svg` | Public Domain | - |
| `static/img/platform/video/dvd.svg` | Public Domain | - |
| `static/img/platform/video/netflix.svg` | Public Domain | - |
| `static/img/platform/video/prime_video.svg` | Public Domain | - |
| `static/img/platform/video/umd.svg` | Public Domain (Embed) | - | 
| `static/img/platform/video/vhs.svg` | Public Domain | - |
| `static/img/platform/video/youtube.svg` | Public Domain | - |
| `static/img/platform/video_game/3do.svg` | All Rights Reserved (Fair Use of) (Embed) | Electronic Arts |
| `static/img/platform/video_game/game_com.svg` | All Rights Reserved (Fair Use of) (Embed) | Tiger Electronics |
| `static/img/platform/video_game/htc_vive.svg` | All Rights Reserved (Fair Use of) | WorldVectorLogo (User: [Siska Meijer](https://worldvectorlogo.com/profile/siska-meijer)); HTC Corporation |
| `static/img/platform/video_game/itv/bell.svg` | Public Domain | - |
| `static/img/platform/video_game/itv/sky_gamestar.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [AxG](https://logos.fandom.com/wiki/User:AxG)) |
| `static/img/platform/video_game/luna.svg` | Public Domain | - |
| `static/img/platform/video_game/ms/xbox.svg` | [CC-0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.en) | - |
| `static/img/platform/video_game/ms/xbox_360.svg` | All Rights Reserved (Fair Use of) | Microsoft Corporation |
| `static/img/platform/video_game/ms/xbox_one.svg` | Public Domain | - |
| `static/img/platform/video_game/ms/xbox_xs.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/3ds.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/64.svg` | All Rights Reserved (Fair Use of) | Nintendo Co., Ltd. |
| `static/img/platform/video_game/nintendo/ds.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/gamecube.svg` | All Rights Reserved (Fair Use of) | Nintendo Co., Ltd. |
| `static/img/platform/video_game/nintendo/gba.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/gbc.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/switch.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/wii.svg` | Public Domain | - |
| `static/img/platform/video_game/nintendo/wii_u.svg` | Public Domain | - |
| `static/img/platform/video_game/oculus.svg` | Public Domain | - |
| `static/img/platform/video_game/sega/dreamcast.svg` | Public Domain | - |
| `static/img/platform/video_game/sega/saturn.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) | Logopedia (User: [Dj152](https://logos.fandom.com/wiki/User:Dj152)) (Original) |
| `static/img/platform/video_game/sony/ps.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/ps2.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/ps3.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/ps4.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/ps5.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/psn--globe.svg` | [CC-BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/) (Embed) | Logopedia (User: [The Jackalope](https://logos.fandom.com/wiki/User:The_Jackalope)) |
| `static/img/platform/video_game/sony/psn--sphere.svg` | Public Domain (Embed) | - |
| `static/img/platform/video_game/sony/psp.svg` | Public Domain | - |
| `static/img/platform/video_game/sony/psvita.svg` | Public Domain | - |
| `static/img/platform/video_game/stadia.svg` | Public Domain | - |
| `static/img/platform/video_game/zeebo.svg` | Public Domain (Embed) | - |
