(() => {
    "use strict";

    const TODO = (msg) => {};

    const timelines = (() => {
        const setup_ = (() => {
            const setupSorters_ = (sorter, entries, sortingFunction) => {
                const input = sorter.querySelector(".tl-js-timeline__sorter--input");
                const options = input.options;

                input.addEventListener("change", () => {
                    const selectedOption = input.options[input.selectedIndex];
                    const selectedValue = selectedOption.value;

                    sortingFunction(selectedValue, entries);
                });
            };

            const findValueGetterFor_ = (() => {
                const valueGetters_ = {
                    "variable-input": (input) => () => input.value,
                    "combo-box": (input) => () => input.options[input.selectedIndex].value
                };

                return (type, input) => valueGetters_[type](input);
            })();

            const findValueEntryParserFor_ = (() => {
                const sanitizer_ = (dirtyValue) => {
                    const dirtyBitStart = dirtyValue.indexOf("{");
                    if (dirtyBitStart == -1) return dirtyValue;
                    return dirtyValue.substring(0, dirtyBitStart);
                }

                const valueEntryParsers_ = {
                    "variable-input": (value) => value,
                    "combo-box": (packedValue) => {
                        if (packedValue.startsWith("extras#")) {
                            const extrasPacked = packedValue.substring("extras#".length);
                            throw new Error("Unable to filter on extras (at the moment)");
                        }
                        if (packedValue.indexOf(",") != -1) {
                            return packedValue.split(",").map(sanitizer_);
                        }
                        return [sanitizer_(packedValue)];
                    }
                };

                return (type) => valueEntryParsers_[type];
            })();

            const findShouldFilterEntryFor_ = (() => {
                const filterHandlers_ = {
                    "variable-input": (filterValue, entryValue) => {
                        TODO("Might want to make this better, but for now, it's sufficient");
                        if (filterValue == "") return true;
                        return entryValue.indexOf(filterValue.toLowerCase()) != -1;
                    },
                    "combo-box": (filterValue, entryValue) => {
                        if (filterValue == "@@all@@") return true;
                        if (filterValue == "") return entryValue == "";
                        return entryValue.indexOf(filterValue) != -1;
                    }
                };

                return (filterType, filterTarget, valueParser, valueGetter) => {
                    const handler = filterHandlers_[filterType];

                    return (entry) => {
                        const packedValue = entry.dataset[filterTarget];
                        const entryValue = valueParser(packedValue);
                        const filterValue = valueGetter();
                        return handler(filterValue, entryValue);
                    }
                }
            })();

            const computeFilterFunction_ = (filter, input) => {
                const filterTarget = filter.dataset.target;
                const type = input.dataset.type;
                const valueGettingFunction = findValueGetterFor_(type, input);
                const valueEntryParsingFunction = findValueEntryParserFor_(type);
                return findShouldFilterEntryFor_(type, filterTarget, valueEntryParsingFunction, valueGettingFunction);
            };

            const computeFilterObject_ = (filter) => {
                const filterInput = filter.querySelector(".tl-js-timeline__filter--input");
                const fun = computeFilterFunction_(filter, filterInput);
                return {
                    input: filterInput,
                    shouldKeepEntry: fun
                }
            };

            const setupFilters_ = (filters, entries, filterFunction) => {
                const filterObjects = Array.from(filters).map(computeFilterObject_);
                filterObjects.forEach(filterObject => {
                    filterObject.input.addEventListener("change", () => filterFunction(entries, filterObjects));
                });
            };

            return {
                setupSorters: setupSorters_,
                setupFilters: setupFilters_
            }
        })();

        const runtime_ = (() => {
            const onSortChange_ = (() => {
                const findOrderingFunctionFor_ = (() => {
                    const orders_ = {
                        isoDate: (numericalOrdering) => (a, b) => {
                            const aSplit = a.split("-");
                            const bSplit = b.split("-");
                            const yearCompare = numericalOrdering(a[0], b[0]);
                            if (yearCompare != 0) return yearCompare;
                            const monthCompare = numericalOrdering(a[1], b[1]);
                            if (monthCompare != 0) return monthCompare;
                            return numericalOrdering(a[2], b[2]);
                        },
                        lexicographical: (a, b) => a.localeCompare(b, "en"),
                        multiNumeric: (numericalOrdering) => (a, b) => {
                            const aNumbers = a.split(";");
                            const bNumbers = b.split(";");
                            const length = aNumbers.length;
                            for (let i = 0; i < length; ++i) {
                                const aNumber = aNumbers[i];
                                const bNumber = bNumbers[i];
                                if (aNumber === undefined) return -1;
                                if (bNumber === undefined) return 1;
                                const compare = numericalOrdering(aNumber, bNumber);
                                if (compare != 0) return compare;
                            }
                            return 0;
                        },
                        numerical: (a, b) => (+a) - (+b)
                    };

                    const isNumeric_ = (x) => !isNaN(x) && x.trim() != "";
                    const isMultiNumeric_ = (x) => x.split(";").every(isNumeric_);
                    const isIsoDate_ = (x) => {
                        const splits = x.split("-");
                        if (splits.length != 3) return false;
                        return isNumeric_(splits[0]) && isNumeric_(splits[1]) && isNumeric_(splits[2]);
                    };

                    return (sortData) => {
                        TODO("Need better 'isNumeric' checks, but this will do for now")
                        if (sortData.every(isNumeric_)) return orders_.numerical;
                        if (sortData.every(isMultiNumeric_)) return orders_.multiNumeric(orders_.numerical);
                        if (sortData.every(isIsoDate_)) return orders_.isoDate(orders_.numerical);
                        return orders_.lexicographical;
                    };
                })();

                return (sorterValue, entries) => {
                    const tableBody = entries[0].parentNode; // There is always one entry in each timeline
                    const entriesArray = Array.from(entries);
                    const sortData = entriesArray.map(entry => entry.dataset[sorterValue]);
                    const orderingFunction = findOrderingFunctionFor_(sortData);
                    entriesArray.sort((a, b) => {
                        const valueA = a.dataset[sorterValue];
                        const valueB = b.dataset[sorterValue];
                        return orderingFunction(valueA, valueB);
                    });
                    entriesArray.forEach(entry => tableBody.appendChild(entry));
                }
            })();

            const onFilterChange_ = (entries, allFilterObjects) => {
                Array.from(entries).forEach(entry => {
                    const shouldKeepInFilter = allFilterObjects.every(filter => filter.shouldKeepEntry(entry));
                    const isFilteredOut = entry.classList.contains("tl-js-timeline__entry--filtered-out");
                    if (shouldKeepInFilter && isFilteredOut) {
                        entry.classList.remove("tl-js-timeline__entry--filtered-out");
                    } else if (!shouldKeepInFilter && !isFilteredOut) {
                        entry.classList.add("tl-js-timeline__entry--filtered-out");
                    }
                });
            };

            return {
                onSortChange: onSortChange_,
                onFilterChange: onFilterChange_
            };
        })();

        return {
            setup: setup_,
            runtime: runtime_
        };
    })();

    window.addEventListener("DOMContentLoaded", () => {
        const sorter = document.querySelector(".tl-js-timeline__sorter");
        const filters = document.querySelectorAll(".tl-js-timeline__filter");
        const entries = document.querySelectorAll(".tl-js-timeline__entry");

        timelines.setup.setupSorters(sorter, entries, timelines.runtime.onSortChange);
        timelines.setup.setupFilters(filters, entries, timelines.runtime.onFilterChange);
    });
})();
