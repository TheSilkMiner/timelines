(() => {
    "use strict";

    const TODO = (msg) => {};

    const checklist = (() => {
        const setup_ = (() => {
            const filterExtrasWithType_ = (extras, type) => {
                const array = [];
                Array.from(extras).forEach(extra => {
                    if (extra.dataset.enabled !== undefined) {
                        const extraDataType = extra.dataset.extraType;
                        if (extraDataType === type) {
                            array.push(extra);
                        }
                    }
                });
                return array;
            };

            const setupCheckboxes_ = (checkboxes, extras, triggerCallback) => {
                Array.from(checkboxes).forEach(checkbox => {
                    const extraType = checkbox.dataset.extraType;
                    const allValidExtras = filterExtrasWithType_(extras, extraType);
                    const input = checkbox.querySelector("input");
                    input.addEventListener('change', event => triggerCallback(() => input.checked, allValidExtras));
                });
            };

            const resolveFunction_ = (() => {
                const behaviorSet = {
                    hide: (h, s) => h,
                    show: (h, s) => s
                }

                return (behavior, hideFun, showFun) => behaviorSet[behavior](hideFun, showFun);
            })();

            const setupTocHideShow_ = (toc, hideFun, showFun) => {
                const hideShow = toc.querySelectorAll(".tl-js-100-checklist--toc-hide-show");
                const tocContent = toc.querySelector(".tl-js-100-checklist__toc-content");
                const currentStatus = toc.dataset.status;
                Array.from(hideShow).forEach(button => {
                    const behavior = button.dataset.behavior;
                    if (currentStatus === behavior) {
                        button.classList.add("tl-js-100-checklist__toc-hide-show--hidden");
                    } else {
                        button.classList.remove("tl-js-100-checklist__toc-hide-show--hidden");
                    }
                    const callback = resolveFunction_(behavior, hideFun, showFun);
                    button.addEventListener('click', event => callback(toc, tocContent, hideShow));
                });
            };

            return {
                setupCheckboxes: setupCheckboxes_,
                setupTocHideShow: setupTocHideShow_
            };
        })();

        const runtime_ = (() => {
            const onCheckboxTriggered_ = (checkStatus, targetedExtras) => {
                const checked = checkStatus()
                targetedExtras.forEach(targetedExtra => {
                    targetedExtra.dataset.enabled = checked;
                    if (checked) {
                        targetedExtra.classList.remove("tl-js-100-checklist__extra--hidden");
                    } else {
                        targetedExtra.classList.add("tl-js-100-checklist__extra--hidden");
                    }
                });
            };

            const tocBehaviors_ = (() => {
                const contentBehaviors_ = (() => {
                    const behaviorSet = {
                        hide: (list, cl) => list.add(cl),
                        show: (list, cl) => list.remove(cl)
                    };

                    const targetClass = "tl-js-100-checklist__toc--hidden";

                    return (behavior, list) => behaviorSet[behavior](list, targetClass);
                })();

                const onTocHideShow_ = (toc, content, hideShowButtons, behavior) => {
                    toc.dataset.status = behavior;
                    Array.from(hideShowButtons).forEach(button => {
                        const buttonBehavior = button.dataset.behavior;
                        if (buttonBehavior === behavior) {
                            button.classList.add("tl-js-100-checklist__toc-hide-show--hidden");
                        } else {
                            button.classList.remove("tl-js-100-checklist__toc-hide-show--hidden");
                        }
                    });
                    contentBehaviors_(behavior, content.classList);
                };

                return {
                    onTocHide: (toc, content, hideShowButtons) => onTocHideShow_(toc, content, hideShowButtons, "hide"),
                    onTocShow: (toc, content, hideShowButtons) => onTocHideShow_(toc, content, hideShowButtons, "show")
                };
            })();

            return {
                onCheckboxTriggered: onCheckboxTriggered_,
                onTocHide: tocBehaviors_.onTocHide,
                onTocShow: tocBehaviors_.onTocShow
            };
        })();

        return {
            setup: setup_,
            runtime: runtime_
        };
    })();

    window.addEventListener("DOMContentLoaded", () => {
        const toc = document.querySelector(".tl-js-100-checklist__toc");
        const checkboxes = document.querySelectorAll(".tl-js-100-checklist__extra-checkbox");
        const extras = document.querySelectorAll(".tl-js-100-checklist__extra");

        checklist.setup.setupTocHideShow(toc, checklist.runtime.onTocHide, checklist.runtime.onTocShow);
        checklist.setup.setupCheckboxes(checkboxes, extras, checklist.runtime.onCheckboxTriggered);
    });
})();
