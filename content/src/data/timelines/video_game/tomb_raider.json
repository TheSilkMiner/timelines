{
  "page_data": {
    "title": "Tomb Raider Timeline",
    "background": "tomb_raider.png",
    "parent_template": "timeline",
    "additional_css": "timeline",
    "additional_js": "timeline"
  },
  "schema_type": "timeline",
  "heading": "Tomb Raider Universe",
  "preamble": [
    "<p>The Tomb Raider franchise is a series of games, movies, and novels centered around the adventures of British",
    "explorer Lara Croft in her adventures around the world. Probably one of the most well-known female characters in",
    "gaming history, the Tomb Raider series has become a synonym of adventure, puzzle elements, and actions in exotic",
    "locations.</p>",
    "<h2>Timeline structure</h2>",
    "<p>The set of Tomb Raider media is extremely convoluted, with various timelines that intersect with each other",
    "and span different <em>universes</em>. In this timeline, we identify four different major timelines, along with",
    "some smaller ones coming usually from the comics or mobile games. These universes are:</p>",
    "<ul><li><strong>Original:</strong> the universe that has been developed originally by Core Design;</li>",
    "<li><strong>Legend:</strong> the universe created by Crystal Dynamics with the Tomb Raider reboot;</li>",
    "<li><strong>Survivor:</strong> the second universe created by Crystal Dynamics following the second Tomb Raider",
    "reboot, starting out with Lara's origin story, and also known as the 'young Lara' universe or, more recently, the",
    "'Unified' universe.</li></ul>",
    "<h2>Reading the timeline</h2>",
    "<p>Due to the presence of the different universes, there are multiple ways of approaching the timeline. The",
    "current structure focuses on release order, but a 'Setting' column is also provided allowing to sort the entries",
    "based on when they take place chronologically. Nevertheless, this approach is also tricky because there is not",
    "necessarily a happens-before relation between elements of different universes. The timeline will be kept",
    "up-to-date if better ordering schemes arise.</p>"
  ],
  "schema": {
    "title": {
      "type": "string",
      "column": "Title",
      "specifier": "title",
      "mandatory": true
    },
    "type": {
      "type": "type",
      "column": "Type",
      "mandatory": true
    },
    "category": {
      "type": "enumeration",
      "column": "Category",
      "valid_values": {
        "": "",
        "animation": "Animated Series",
        "arcade_promo": "Arcade Game",
        "browser_promo": "Browser Game",
        "console": "PC/Console Game",
        "dark_horse": "Dark Horse",
        "itv": "Interactive TV",
        "mobile": "Mobile Game",
        "slot": "Slot Machine",
        "top_cow": "Top Cow",
        "vr_promo": "VR Game"
      }
    },
    "platform": {
      "type": "platform",
      "column": "Platform",
      "specifier": "platform"
    },
    "universe": {
      "type": "enumeration",
      "column": "Universe",
      "valid_values": {
        "": "",
        "original": "Original",
        "legend": "Legend",
        "survivor": "Survivor / Unified",
        "spin_off": "Spin-off",
        "gbc": "GameBoy Color",
        "mobile_trilogy": "Mobile Trilogy",
        "movie_original": "Original (Movie)",
        "movie_reboot": "Reboot (Movie)",
        "top_cow_original": "Original (Top Cow)",
        "top_cow_alternative": "Alternative (Top Cow)",
        "rev_tv": "Revisioned",
        "nf_tv": "Netflix"
      },
      "mandatory": true
    },
    "period": {
      "type": "date_year",
      "column": "Setting",
      "filterable": false,
      "sortable": true
    },
    "date": {
      "type": "date_iso",
      "column": "Release Date",
      "filterable": false,
      "sortable": true,
      "specifier": "date",
      "mandatory": true
    },
    "extras": {
      "type": "extras",
      "column": "Extras",
      "filterable": false
    },
    "notes": {
      "type": "string",
      "column": "Notes",
      "filterable": false,
      "specifier": "notes"
    }
  },
  "data": [
    {
      "title": "Tomb Raider",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sega_saturn",
        "sony_playstation",
        "pc_dos",
        "ms_pocket_pc{xp}",
        "nokia_n_gage",
        "pc_win{xp}",
        "sony_playstation_network{globe}",
        "ios",
        "android"
      ],
      "universe": "original",
      "period": 1996,
      "date": "1996-10-25",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb1.html"
        }
      ],
      "notes": "Beginning of the Core Design era"
    },
    {
      "title": "Tomb Raider II: Starring Lara Croft",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_win{95}",
        "sony_playstation",
        "pc_mac{os}",
        "ios",
        "android"
      ],
      "universe": "original",
      "period": 1997,
      "date": "1997-11-21",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb2.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Trilogy",
      "type": "short",
      "category": "",
      "platform": [
        "youtube"
      ],
      "universe": "original",
      "period": 1998,
      "date": "1998-10-15",
      "extras": [
        {
          "type": "watch",
          "title": "Watch on YouTube",
          "url": "https://youtu.be/91SoddPBJG8"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider III: Adventures of Lara Croft",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_win{95}",
        "sony_playstation",
        "pc_mac{os}"
      ],
      "universe": "original",
      "period": 1998,
      "date": "1998-11-20",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb3.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Unfinished Business",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_dos",
        "pc_win{95}",
        "pc_mac{os}",
        "ios",
        "android"
      ],
      "universe": "original",
      "period": 1996,
      "date": "1998-12-31",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/goldwalk.html"
        }
      ],
      "notes": "Mac release of 'Tomb Raider'"
    },
    {
      "title": "Tomb Raider II: Golden Mask",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_win{95}",
        "pc_mac{os}"
      ],
      "universe": "original",
      "period": 1997,
      "date": "1999-06-04",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/goldmask.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider Collectible Card Game",
      "type": "board_game",
      "category": "",
      "platform": [],
      "universe": "",
      "period": 1999,
      "date": "1999-08-01",
      "extras": [],
      "notes": "Inexact release date"
    },
    {
      "title": "Tomb Raider: The Last Revelation",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation",
        "pc_win{95}",
        "sega_dreamcast",
        "pc_mac{os}"
      ],
      "universe": "original",
      "period": 1999,
      "date": "1999-11-19",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb4.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Series",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 1999,
      "date": "1999-12-01",
      "extras": [],
      "notes": "50 issues between 1999 and 2005"

    },
    {
      "title": "Tomb Raider: The Times",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_win{95}"
      ],
      "universe": "original",
      "period": 1999,
      "date": "1999-12-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb4.html#bonus"
        }
      ],
      "notes": "Bonus level for 'The Last Revelation'"
    },
    {
      "title": "Tomb Raider III: The Lost Artefact",
      "type": "video_game",
      "category": "console",
      "platform": [
        "pc_win{95}",
        "pc_mac{os}"
      ],
      "universe": "original",
      "period": 1998,
      "date": "2000-03-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/lostartifact.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Origins",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2000,
      "date": "2000-07-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "nintendo_gbc"
      ],
      "universe": "gbc",
      "period": 2000,
      "date": "2000-07-06",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trgameboy.html#intro"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Chronicles",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation",
        "pc_win{95}",
        "sega_dreamcast",
        "pc_mac{osx}"
      ],
      "universe": "original",
      "period": 1995,
      "date": "2000-11-17",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb5.html"
        }
      ],
      "notes": "Additional settings are 1998, 1984, and 1997"
    },
    {
      "title": "The Tomb Raider Gallery",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2000,
      "date": "2000-12-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider: Journeys",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_alternative",
      "period": 2001,
      "date": "2001-01-01",
      "extras": [],
      "notes": "12 issues between 2001 and 2003"
    },
    {
      "title": "Lara's Land Rover Challenge",
      "type": "video_game",
      "category": "browser_promo",
      "platform": [
        "flash"
      ],
      "universe": "",
      "period": 2001,
      "date": "2001-04-01",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Lara Croft: Tomb Raider",
      "type": "movie",
      "category": "",
      "platform": [],
      "universe": "movie_original",
      "period": 2001,
      "date": "2001-06-15",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Curse of the Sword",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "nintendo_gbc"
      ],
      "universe": "gbc",
      "period": 2001,
      "date": "2001-07-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trgameboy.html#intro"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Prophecy",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "nintendo_gba"
      ],
      "universe": "",
      "period": 2002,
      "date": "2002-11-12",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trgameboy.html#gba-intro"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Apocalypse - The Eye of Osiris",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_sky_gamestar"
      ],
      "universe": "",
      "period": 2002,
      "date": "2002-12-18",
      "extras": [],
      "notes": "Episode 1"
    },
    {
      "title": "Tomb Raider: Scarface's Treasure",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2003,
      "date": "2003-04-01",
      "extras": [],
      "notes": "1 issue; inexact release date"
    },
    {
      "title": "Tomb Raider: The Osiris Codex",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me",
        "exen"
      ],
      "universe": "mobile_trilogy",
      "period": 2003,
      "date": "2003-05-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#trilogy"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Apocalypse - The Shadow Falls",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_sky_gamestar"
      ],
      "universe": "",
      "period": 2003,
      "date": "2003-06-16",
      "extras": [],
      "notes": "Episode 2"
    },
    {
      "title": "Tomb Raider: The Angel of Darkness",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_2",
        "pc_win{xp}",
        "pc_mac{osx}"
      ],
      "universe": "original",
      "period": 2003,
      "date": "2003-06-30",
      "extras":  [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb6.html"
        }
      ],
      "notes": "End of the Core Design era"
    },
    {
      "title": "Lara Croft Tomb Raider: The Cradle of Life",
      "type": "movie",
      "category": "",
      "platform": [],
      "universe": "movie_original",
      "period": 2003,
      "date": "2003-07-25",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Epiphany",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2003,
      "date": "2003-08-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider: Apocalypse - The Last Midnight",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_sky_gamestar"
      ],
      "universe": "",
      "period": 2003,
      "date": "2003-09-23",
      "extras": [],
      "notes": "Episode 3"
    },
    {
      "title": "The Amulet of Power",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "original",
      "period": 2003,
      "date": "2003-12-02",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Takeover",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2004,
      "date": "2004-01-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider",
      "type": "video_game",
      "category": "browser_promo",
      "platform": [
        "flash"
      ],
      "universe": "",
      "period": 2003,
      "date": "2004-01-04",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Tomb Raider: Sphere of Influence",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2004,
      "date": "2004-03-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider: Quest for Cinnabar",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me",
        "exen"
      ],
      "universe": "mobile_trilogy",
      "period": 2004,
      "date": "2004-05-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#trilogy"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Arabian Nights",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2004,
      "date": "2004-08-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "The Lost Cult",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "original",
      "period": 2004,
      "date": "2004-08-03",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Elixir of Life",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me",
        "exen"
      ],
      "universe": "mobile_trilogy",
      "period": 2004,
      "date": "2004-10-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#trilogy"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider",
      "type": "gambling_game",
      "category": "slot",
      "platform": [],
      "universe": "",
      "period": 2004,
      "date": "2004-10-01",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Man of Bronze",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "original",
      "period": 2004,
      "date": "2004-12-28",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Apocalypse - The Temple of Anubis",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_sky_gamestar"
      ],
      "universe": "",
      "period": 2005,
      "date": "2005-07-01",
      "extras": [],
      "notes": "Episode 4"
    },
    {
      "title": "Tomb Raider: The Greatest Treasure of All",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2005,
      "date": "2005-10-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider: The Reckoning (Ep 1)",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_bell"
      ],
      "universe": "",
      "period": 2006,
      "date": "2006-03-01",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider Cover Gallery",
      "type": "comic",
      "category": "top_cow",
      "platform": [],
      "universe": "top_cow_original",
      "period": 2006,
      "date": "2006-04-01",
      "extras": [],
      "notes": "1 issue"
    },
    {
      "title": "Tomb Raider: Legend",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_2",
        "ms_xbox",
        "ms_xbox_360",
        "pc_win{xp}",
        "sony_playstation_portable",
        "nintendo_gba",
        "nintendo_ds",
        "nintendo_gamecube",
        "sony_playstation_3"
      ],
      "universe": "legend",
      "period": 2006,
      "date": "2006-04-07",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb7.html"
        }
      ],
      "notes": "Beginning of the Crystal Dynamics era"
    },
    {
      "title": "Lara Croft's Poker Party",
      "type": "gambling_game",
      "category": "mobile",
      "platform": [
        "j2me"
      ],
      "universe": "",
      "period": 2006,
      "date": "2006-06-13",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Puzzle Paradox",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me"
      ],
      "universe": "",
      "period": 2006,
      "date": "2006-07-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#paradox"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Reckoning (Ep 2)",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "itv_bell"
      ],
      "universe": "",
      "period": 2006,
      "date": "2006-09-01",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: The Action Adventure",
      "type": "video_game",
      "category": "itv",
      "platform": [
        "dvd"
      ],
      "universe": "",
      "period": 2006,
      "date": "2006-11-21",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Legend",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me"
      ],
      "universe": "legend",
      "period": 2006,
      "date": "2006-12-31",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#legend"
        }
      ],
      "notes": "Port of 'Tomb Raider: Legend'"
    },
    {
      "title": "Trail Rider",
      "type": "video_game",
      "category": "browser_promo",
      "platform": [
        "flash"
      ],
      "universe": "legend",
      "period": 2006,
      "date": "2006-12-31",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Tomb Raider: Anniversary",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_2",
        "pc_win{vista}",
        "sony_playstation_portable",
        "ms_xbox_360",
        "nintendo_wii",
        "pc_mac{osx}",
        "sony_playstation_3"
      ],
      "universe": "legend",
      "period": 1996,
      "date": "2007-06-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/anniversary.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Revisioned: Tomb Raider Animated Series",
      "type": "tv_series",
      "category": "animation",
      "platform": [],
      "universe": "rev_tv",
      "period": 2007,
      "date": "2007-07-10",
      "extras": [],
      "notes": "10 episodes"
    },
    {
      "title": "Tomb Raider: Anniversary",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me"
      ],
      "universe": "legend",
      "period": 1996,
      "date": "2007-12-01",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#anniversary"
        }
      ],
      "notes": "Port of 'Tomb Raider: Anniversary'"
    },
    {
      "title": "Tomb Raider: Secret of the Sword",
      "type": "gambling_game",
      "category": "slot",
      "platform": [],
      "universe": "",
      "period": 2008,
      "date": "2008-03-10",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Underworld",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_2",
        "sony_playstation_3",
        "pc_mac{osx}",
        "pc_win{vista}",
        "nokia_n_gage",
        "nintendo_ds",
        "nintendo_wii",
        "ms_xbox_360"
      ],
      "universe": "legend",
      "period": 2007,
      "date": "2008-11-21",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb8.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Underworld",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "j2me"
      ],
      "universe": "legend",
      "period": 2007,
      "date": "2008-11-21",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#underworld"
        }
      ],
      "notes": "Port of 'Tomb Raider: Underworld'"
    },
    {
      "title": "Tomb Raider: Underworld: Beneath the Ashes",
      "type": "video_game",
      "category": "console",
      "platform": [
        "ms_xbox_360",
        "ms_xbox_one"
      ],
      "universe": "legend",
      "period": 2007,
      "date": "2009-02-10",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb8.html#bonus"
        }
      ],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Underworld: Lara's Shadow",
      "type": "video_game",
      "category": "console",
      "platform": [
        "ms_xbox_360",
        "ms_xbox_one"
      ],
      "universe": "legend",
      "period": 2007,
      "date": "2009-03-10",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb8.html#bonus"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft and the Guardian of Light",
      "type": "video_game",
      "category": "console",
      "platform": [
        "ms_xbox_360",
        "pc_win{vista}",
        "sony_playstation_3",
        "ios",
        "android"
      ],
      "universe": "spin_off",
      "period": 2010,
      "date": "2010-08-18",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/guardian.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "The Tomb Raider Trilogy",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_3"
      ],
      "universe": "legend",
      "period": 2007,
      "date": "2011-03-22",
      "extras": [],
      "notes": "Remake of the 'Legend' timeline"
    },
    {
      "title": "Tomb Raider",
      "type": "comic",
      "category": "dark_horse",
      "platform": [],
      "universe": "survivor",
      "period": 2013,
      "date": "2013-01-01",
      "extras": [],
      "notes": "Multiple issues for different story arcs"
    },
    {
      "title": "Tomb Raider",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_3",
        "pc_win{8}",
        "ms_xbox_360",
        "pc_mac{osx}",
        "pc_linux"
      ],
      "universe": "survivor",
      "period": 2013,
      "date": "2013-03-05",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb9.html"
        }
      ],
      "notes": "Also known as 'Tomb Raider: A Survivor is Born'"
    },
    {
      "title": "Lara Croft: Reflections",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "ios"
      ],
      "universe": "",
      "period": 2013,
      "date": "2013-12-20",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Definitive Edition",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_4",
        "ms_xbox_one",
        "stadia"
      ],
      "universe": "survivor",
      "period": 2013,
      "date": "2014-01-28",
      "extras": [],
      "notes": "Remake of 'Tomb Raider'"
    },
    {
      "title": "Tomb Raider: The Ten Thousand Immortals",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "survivor",
      "period": 2014,
      "date": "2014-10-20",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Lara Croft and the Temple of Osiris",
      "type": "video_game",
      "category": "console",
      "platform": [
        "ms_xbox_one",
        "pc_win{8}",
        "sony_playstation_4"
      ],
      "universe": "spin_off",
      "period": 2014,
      "date": "2014-12-09",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/temple.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft: Relic Run",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "ios",
        "android",
        "ms_win_phone{8}"
      ],
      "universe": "",
      "period": 2015,
      "date": "2015-05-28",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#lc-relic-run"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft GO",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "sony_playstation_4",
        "sony_playstation_vita",
        "ios",
        "android",
        "ms_win_phone{8}",
        "pc_win{8}"
      ],
      "universe": "",
      "period": 2015,
      "date": "2015-08-27",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/trmobile.html#lara-croft-go"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft and the Frozen Omen",
      "type": "comic",
      "category": "dark_horse",
      "platform": [],
      "universe": "legend",
      "period": 2015,
      "date": "2015-10-01",
      "extras": [],
      "notes": "5 issues between 2015 and 2016"
    },
    {
      "title": "Rise of the Tomb Raider",
      "type": "video_game",
      "category": "console",
      "platform": [
        "ms_xbox_360",
        "ms_xbox_one",
        "sony_playstation_4",
        "pc_win{10}",
        "pc_mac{macos}",
        "pc_linux",
        "stadia"
      ],
      "universe": "survivor",
      "period": 2015,
      "date": "2015-11-10",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb10.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft and the Blade of Gwynnever",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "",
      "period": 2016,
      "date": "2016-07-26",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider: Escape the Tomb",
      "type": "video_game",
      "category": "browser_promo",
      "platform": [],
      "universe": "",
      "period": 2018,
      "date": "2018-01-23",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Tomb Raider VR: Lara's Escape",
      "type": "video_game",
      "category": "vr_promo",
      "platform": [
        "oculus"
      ],
      "universe": "",
      "period": 2018,
      "date": "2018-02-20",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Tomb Raider",
      "type": "video_game",
      "category": "arcade_promo",
      "platform": [],
      "universe": "",
      "period": 2018,
      "date": "2018-03-01",
      "extras": [],
      "notes": "Promotion for the movie"
    },
    {
      "title": "Tomb Raider",
      "type": "movie",
      "category": "",
      "platform": [],
      "universe": "movie_reboot",
      "period": 2018,
      "date": "2018-03-16",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Shadow of the Tomb Raider: Path of the Stars",
      "type": "video_game",
      "category": "browser_promo",
      "platform": [],
      "universe": "survivor",
      "period": 2018,
      "date": "2018-03-19",
      "extras": [],
      "notes": "Five puzzles released one week apart from each other"
    },
    {
      "title": "Shadow of the Tomb Raider: Path of the Apocalypse",
      "type": "novel",
      "category": "",
      "platform": [],
      "universe": "survivor",
      "period": 2018,
      "date": "2018-09-14",
      "extras": [],
      "notes": "Prequel to the game"
    },
    {
      "title": "Shadow of the Tomb Raider",
      "type": "video_game",
      "category": "console",
      "platform": [
        "sony_playstation_4",
        "ms_xbox_one",
        "pc_win{10}",
        "stadia"
      ],
      "universe": "survivor",
      "period": 2018,
      "date": "2018-09-14",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide",
          "url": "https://tombraiders.net/stella/tomb11.html"
        }
      ],
      "notes": ""
    },
    {
      "title": "Lara Croft: Temples and Tombs",
      "type": "gambling_game",
      "category": "slot",
      "platform": [],
      "universe": "",
      "period": 2019,
      "date": "2019-05-08",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider Legends",
      "type": "board_game",
      "category": "",
      "platform": [],
      "universe": "",
      "period": 2019,
      "date": "2019-06-21",
      "extras": [],
      "notes": ""
    },
    {
      "title": "Tomb Raider Reloaded",
      "type": "video_game",
      "category": "mobile",
      "platform": [
        "ios",
        "android",
        "netflix"
      ],
      "universe": "",
      "period": 1996,
      "date": "2020-12-03",
      "extras": [],
      "notes": "Global rollout from 2022-09-29"
    },
    {
      "title": "Tomb Raider I-III Remastered Starring Lara Croft",
      "type": "video_game",
      "category": "console",
      "platform": [
        "nintendo_switch",
        "pc_win{11}",
        "sony_playstation_4",
        "sony_playstation_5",
        "ms_xbox_one",
        "ms_xbox_xs"
      ],
      "universe": "original",
      "period": 1996,
      "date": "2024-02-14",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR1)",
          "url": "https://tombraiders.net/stella/tomb1.html"
        },
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR2)",
          "url": "https://tombraiders.net/stella/tomb2.html"
        },
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR3)",
          "url": "https://tombraiders.net/stella/tomb3.html"
        }
      ],
      "notes": "Remaster of TR1, TR2, TR3"
    },
    {
      "title": "Tomb Raider: The Legend of Lara Croft",
      "type": "tv_series",
      "category": "",
      "platform": [
        "netflix"
      ],
      "universe": "survivor",
      "period": 2024,
      "date": "2024-10-10",
      "extras": [],
      "notes": "8 episodes in season 1; time period is an educated guess based on available information"
    },
    {
      "title": "Tomb Raider IV-VI Remastered Starring Lara Croft",
      "type": "video_game",
      "category": "console",
      "platform": [
        "nintendo_switch",
        "pc_win{11}",
        "sony_playstation_4",
        "sony_playstation_5",
        "ms_xbox_one",
        "ms_xbox_xs"
      ],
      "universe": "original",
      "period": 1999,
      "date": "2025-02-14",
      "extras": [
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR:TLR/TR4)",
          "url": "https://tombraiders.net/stella/tomb4.html"
        },
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR:C/TR5)",
          "url": "https://tombraiders.net/stella/tomb5.html"
        },
        {
          "type": "guide",
          "title": "Stella's Tomb Raiders Guide (TR:AOD/TR6)",
          "url": "https://tombraiders.net/stella/tomb6.html"
        }
      ],
      "notes": "Remaster of TR:TLR (TR4), TR:C (TR5), TR:AOD (TR6)"
    },
    {
      "title": "Tomb Raider: The Crypt of Chronos",
      "type": "board_game",
      "category": "",
      "platform": [],
      "universe": "",
      "period": 9999,
      "date": "2025-05-31",
      "extras": [],
      "notes": "Upcoming; information is tentative"
    },
    {
      "title": "Tomb Raider",
      "type": "tv_series",
      "category": "",
      "platform": [],
      "universe": "survivor",
      "period": 9999,
      "date": "9999-12-31",
      "extras": [],
      "notes": "Upcoming; universe is tentative; Amazon"
    },
    {
      "title": "&lt;unknown&gt;",
      "type": "movie",
      "category": "",
      "platform": [],
      "universe": "survivor",
      "period": 9999,
      "date": "9999-12-31",
      "extras": [],
      "notes": "Upcoming; universe is tentative"
    },
    {
      "title": "&lt;unknown&gt;",
      "type": "video_game",
      "category": "",
      "platform": [],
      "universe": "survivor",
      "period": 9999,
      "date": "9999-12-31",
      "extras": [],
      "notes": "Upcoming"
    }
  ],
  "todo": {
    "Tomb Raider: The Series comic": [
      "Expand all the series into entries: refer to 'https://www.comics.org/series/18524/'"
    ],
    "Tomb Raider: Journeys comic": [
      "Expand all the series into entries: refer to 'https://www.comics.org/series/23361/'"
    ],
    "Tomb Raider Dark Horse comic": [
      "Split into different issues and into arcs. See 'https://www.comics.org/searchNew/?q=tomb%20raider%20dark%20horse', wiki, and 'Tomb_Raider:_The_Beginning'"
    ],
    "Tomb Raider: Scarface's Treasure": [
      "https://web.archive.org/web/20090727093135/http://forum.newsarama.com/showthread.php?t=4194",
      "The above is the only mention of some sort of release date, located in April; everything else just gives even more conflicting info"
    ],
    "Lara Croft and the Frozen Omen comic": [
      "Split into different issues. See https://www.comics.org/series/92956/'"
    ],
    "Tomb Raider: Legend game": [
      "Verify if the 'Setting' is correct"
    ],
    "Tomb Raider: Underworld game": [
      "Verify if the 'Setting' is correct"
    ],
    "Shadow of the Tomb Raider: Path to the Stars browser promo": [
      "Split the various episodes up?"
    ],
    "All movies": [
      "Add platforms that allow for streaming"
    ],
    "Revisioned animated series": [
      "Expand series into entries; also add where you can watch the series"
    ],
    "All promotional material for Tomb Raider reboot movie": [
      "Movie is loosely based on 2013 game, so that's probably the time period?"
    ]
  }
}
