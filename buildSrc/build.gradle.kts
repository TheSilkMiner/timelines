plugins {
    `groovy-gradle-plugin`
}

repositories {
    mavenCentral()
}

dependencies {
    gradleApi()
}

gradlePlugin {
    plugins {
        create("content-plugin") {
            id = "net.thesilkminer.timelines.content-plugin"
            implementationClass = "net.thesilkminer.timelines.gradle.content.ContentPlugin"
        }
        create("website-plugin") {
            id = "net.thesilkminer.timelines.website-plugin"
            implementationClass = "net.thesilkminer.timelines.gradle.website.WebsitePlugin"
        }
    }
}
