package net.thesilkminer.timelines.gradle.content

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer

class ContentPlugin implements Plugin<Project> {
    @Override
    void apply(final Project target) {
        applyAndFixJavaPlugin(target)
    }

    // Not really a Java project, but I need a JVM plugin to be able to work with source-sets
    private static applyAndFixJavaPlugin(final Project project) {
        project.plugins.apply 'org.gradle.java'

        final javaConvention = project.extensions.getByType JavaPluginExtension
        final sourceSets = javaConvention.sourceSets

        project.afterEvaluate {
            project.pluginManager.withPlugin('org.gradle.java') {
                final targets = addSourceSets sourceSets
                clearSourceSets sourceSets
                fixMainSourceSets sourceSets, targets
            }
        }
    }


    private static addSourceSets(final SourceSetContainer sourceSets) {
        return ["static", "template", "data"].each(sourceSets.&create)
    }

    private static clearSourceSets(final SourceSetContainer sourceSets) {
        sourceSets.getAsMap().values().each(ContentPlugin.&clearSourceSet)
    }

    private static fixMainSourceSets(final SourceSetContainer sourceSets, final targets) {
        targets.each { sourceSets.getByName(it).resources.srcDirs = ["src/$it"] }
    }

    private static clearSourceSet(final SourceSet set) {
        [set.java, set.resources].each { it.srcDirs = [] }
    }
}
