package net.thesilkminer.timelines.gradle.website

import groovy.transform.PackageScope
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension

@PackageScope
class Util {
    static final SITE_ROOT = ''

    static findSubproject(final Project project, final String name) {
        project.project name
    }

    static findResourceDirectoryFor(final Project project, final String name) {
        findSourceSet(project, name).resources.srcDirs.find()
    }

    static findSourceSet(final Project project, final String name) {
        project.extensions.getByType(JavaPluginExtension).sourceSets.findByName(name)
    }

    static findRuntimeClasspath(final Project project) {
        findSourceSet(project, 'main').runtimeClasspath
    }
}
