package net.thesilkminer.timelines.gradle.website

import groovy.transform.PackageScope
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.gradle.process.JavaExecSpec

@PackageScope
class RunTestServerTask extends DefaultTask {
    @TaskAction
    run() {
        final testServer = this.project.project 'test-server'
        final serverRuntime = Util.findRuntimeClasspath testServer
        final outDir = this.project.file "${ -> this.project.buildDir }/website"

        def result = this.project.javaexec { JavaExecSpec jes ->
            jes.classpath = serverRuntime
            jes.mainClass.set 'net.thesilkminer.timelines.server.0_Main'
            jes.args = [
                    outDir.path,
                    Util.SITE_ROOT
            ]
        }
        result.rethrowFailure()
        result.assertNormalExitValue()
    }
}
