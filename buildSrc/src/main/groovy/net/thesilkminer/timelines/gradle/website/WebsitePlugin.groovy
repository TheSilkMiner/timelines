package net.thesilkminer.timelines.gradle.website

import org.gradle.api.Plugin
import org.gradle.api.Project

@SuppressWarnings("unused")
class WebsitePlugin implements Plugin<Project> {

    @Override
    void apply(final Project target) {
        final buildTask = defineBuildingTasks target
        defineTestingTasks target, buildTask
    }

    private static defineBuildingTasks(final Project project) {
        final buildWebsite = project.tasks.create('buildWebsite', BuildWebsiteTask) {
            it.group = 'build'
        }

        project.afterEvaluate {
            final build = project.tasks.findByName('build') ?: {
                println 'No build task found for the root project, creating one'
                project.tasks.create('build') {
                    it.group = 'build'
                }
            }()
            build.dependsOn buildWebsite
        }

        project.project('ssg').afterEvaluate {
            buildWebsite.dependsOn(project.project('ssg').tasks.getByName('compileGroovy'))
        }

        buildWebsite
    }

    private static defineTestingTasks(final Project project, final buildWebsiteTask) {
        final runServer = project.tasks.create('testWebsite', RunTestServerTask) {
            it.group = 'verification'
            it.dependsOn buildWebsiteTask
        }

        project.project('test-server').afterEvaluate {
            runServer.dependsOn(project.project('test-server').tasks.getByName('compileKotlin'))
        }
    }
}
