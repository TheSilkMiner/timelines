package net.thesilkminer.timelines.gradle.website

import groovy.transform.PackageScope
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.tasks.TaskAction
import org.gradle.process.JavaExecSpec

@PackageScope
class BuildWebsiteTask extends DefaultTask {

    @TaskAction
    build() {
        final ssgProject = Util.findSubproject this.project, 'ssg'
        final templateProject = Util.findSubproject  this.project, 'content'

        runSsgOnTemplate ssgProject, templateProject
    }

    private runSsgOnTemplate(final Project ssg, final Project content) {
        final dataSet = Util.findResourceDirectoryFor content, 'data'
        final staticSet = Util.findResourceDirectoryFor content, 'static'
        final templateSet = Util.findResourceDirectoryFor content, 'template'
        final outDir = this.project.file "${ -> this.project.buildDir }/website"
        final ssgRuntime = Util.findRuntimeClasspath(ssg)

        def result = this.project.javaexec { JavaExecSpec jes ->
            jes.classpath = ssgRuntime
            jes.mainClass.set 'net.thesilkminer.timelines.ssg.Main'
            jes.args = [
                    dataSet.path,
                    staticSet.path,
                    templateSet.path,
                    outDir.path,
                    Util.SITE_ROOT
            ]
        }
        result.rethrowFailure()
        result.assertNormalExitValue()
    }
}
